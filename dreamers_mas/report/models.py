from datetime import timedelta

from campaign.models import get_all_purchases_of_campaign
from dreamers_mas.models import *


def get_sales(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--" \
                                                                                    "(con:Consumer)-[r:BUYS]->(clo:Clothes) return sum(r.quantity)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("get_sales: " + repr(e))
        raise


def cal_revenue(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--" \
                                                                                    "(con:Consumer)-[r:BUYS]->(clo:Clothes) with r.quantity*r.price as re return sum(re)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("cal_revenue: " + repr(e))
        raise


def get_top_five_consumer(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--" \
                                                                                    "(con:Consumer)-[r:BUYS]->(clo:Clothes) " \
                                                                                    "with r.quantity*r.price as re, con.con_id as id, r.quantity as quan, con.name as name, " \
                                                                                    "con.gender as gender " \
                                                                                    "return id, name, sum(quan) as num_buy, sum(re) as total, gender order by total desc limit 5"
        results, meta = db.cypher_query(query)
        return results
    except Exception as e:
        logging.getLogger("error_logger").error("get_top_five_consumer: " + repr(e))
        raise


def get_num_zalo(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--" \
                                                                                    "(con:Consumer) where con.zalo_id <> \"\" return count(con.zalo_id)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("get_num_zalo: " + repr(e))
        return 0


def get_num_sms(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--" \
                                                                                    "(con:Consumer) where con.phone_number <> \"\" return count(con.phone_number)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("get_num_sms: " + repr(e))
        return 0


def get_num_email(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--" \
                                                                                    "(con:Consumer) where con.email <> \"\" return count(con.email)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("get_num_email: " + repr(e))
        return 0


def get_num_messenger(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--" \
                                                                                    "(con:Consumer) where con.facebook_id <> \"\" return count(con.facebook_id)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("get_num_messenger: " + repr(e))
        return 0


def get_reports_by_campaign(campaign):
    try:
        report_data = campaign.report.all()
        return report_data
    except Exception as e:
        # logging.getLogger("error_logger").error("get_reports_by_campaign: " + repr(e))
        return []


def get_average_recall(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--(r:Report) return avg(r.recall)"
        results, meta = db.cypher_query(query)
        avg = results[0][0]
        if avg is None:
            return 0
        return avg
    except Exception as e:
        logging.getLogger("error_logger").error("get_average_recall: " + repr(e))
        return 0


def get_sale_last_30days(user, campaign):
    try:
        query = "match(u:User{uid:'" + user.uid + "'})--(c:Campaign{cam_id:'" + campaign.cam_id + "'})" \
                                                                                                  "--(con:Consumer)-[r:BUYS]->(clo:Clothes) return r order by r.purchase_time desc"
        results, meta = db.cypher_query(query)
        purs = [PurchaseHistory.inflate(row[0]) for row in results]
        count_day = 0
        sale_data = {}
        sale_date = purs[0].purchase_time.strftime("%Y-%m-%d")
        num_sales = 0
        for p in purs:
            if p.purchase_time.strftime("%Y-%m-%d") == sale_date:
                num_sales += p.quantity
            else:
                sale_data[sale_date] = num_sales
                sale_date = p.purchase_time.strftime("%Y-%m-%d")
                num_sales = p.quantity
                count_day += 1
            if count_day > 30:
                break
        return sale_data
    except Exception as e:
        # logging.getLogger("error_logger").error("get_current_package_of_user: " + repr(e))
        return {}
