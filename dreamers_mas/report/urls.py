from django.urls import path
from . import views

urlpatterns = [
    path('<cam_id>/', views.report, name='report'),
]