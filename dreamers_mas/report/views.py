from django.shortcuts import render
from campaign.models import *
from dashboard.views import check_expire
from dreamers_mas.decorators import user_login_required
from homepage.models import get_user_by_uid
from .models import *


@user_login_required
def report(request, cam_id):
    check_expire(request)
    uid = request.session['uid']
    user = get_user_by_uid(uid)

    campaign = get_campaign_of_user(cam_id, uid)
    sales = get_sales(uid, cam_id)
    consumers = count_all_consumers_of_campaign(uid, cam_id)
    revenue = cal_revenue(uid, cam_id)
    avg_recall = get_average_recall(uid, cam_id)
    top_5 = get_top_five_consumer(uid, cam_id)
    num_zalo = get_num_zalo(uid, cam_id)
    num_sms = get_num_sms(uid, cam_id)
    num_email = get_num_email(uid, cam_id)
    num_messenger = get_num_messenger(uid, cam_id)
    channel_data = {
        "zalo": num_zalo,
        "sms": num_sms,
        "email": num_email,
        "messenger": num_messenger,
    }
    ai_data = make_ai_data(user, campaign)
    context = {
        'campaign': campaign,
        'ai_data': ai_data,
        'sales': sales,
        'consumers': consumers,
        'revenue': revenue,
        'avg_recall': avg_recall*100,
        'top_5': top_5,
        'channel_data': channel_data,
    }
    return render(request, 'report.html', context)


def make_ai_data(user, campaign):
    try:
        report_data = get_reports_by_campaign(campaign)
        sale_data = get_sale_last_30days(user, campaign)
        ai_data = []
        if len(report_data) > 30:
            date_range = 30
        else:
            date_range = len(report_data)
        for index, r in zip(range(date_range), report_data):
            report_date = r.create_date.strftime("%Y-%m-%d")
            try:
                sale = sale_data[report_date]
                if sale is None:
                    sale = 0
            except Exception as e:
                logging.getLogger("error_logger").error("set sale 0: " + repr(e))
                sale = 0
                pass
            ai_data.insert(0, {
                "date": report_date,
                "sale": sale,
                "recommend": r.num_recommend,
                "recall": r.recall*100,
                "precision": r.precision*100
            })

        return ai_data
    except Exception as e:
        logging.getLogger("error_logger").error("make_ai_data: " + repr(e))
