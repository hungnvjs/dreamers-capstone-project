import json
import logging
from datetime import datetime

from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render
from django.urls import reverse

from dashboard.models import get_info_of_user
from dreamers_mas.decorators import user_login_required

# Create your views here.
from homepage.models import get_user_by_uid, get_user_by_username
from .models import *


@user_login_required
def inbox(request):
    try:
        context = load_inbox(request)
        if request.is_ajax():
            return JsonResponse(context)
            # json_data = json.dumps(context)
            # return HttpResponse(json_data, content_type='application/json')
        else:
            return render(request, 'inbox.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("inbox. " + repr(e))
        messages.error(request, "Can't list subject!")
        return render(request, 'inbox.html')


def load_inbox(request):
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)

        all_subjects = get_all_subjects(user)
        mess_data = []
        if all_subjects is not None and len(all_subjects) > 0:
            for subject in all_subjects:
                all_mess = get_all_messages_of_subject(subject.sub_id)
                if len(all_mess) > 0:
                    last_mess = all_mess[-1]
                    if last_mess.receiver_username == user.username:
                        partner = last_mess.sender_username
                    else:
                        partner = last_mess.receiver_username

                    now = datetime.now()
                    if now.year == last_mess.send_date.year:
                        if now.month == last_mess.send_date.month and now.day == last_mess.send_date.day:
                            mess_time = datetime.strftime(last_mess.send_date, "%I:%M %p")
                        else:
                            mess_time = datetime.strftime(last_mess.send_date, "%b %d")
                    else:
                        mess_time = datetime.strftime(last_mess.send_date, "%Y-%m")

                    sub_dict = {
                        'subject': subject.dumps(),
                        'num_mess': len(all_mess),
                        'last_mess': last_mess.dumps(),
                        'partner': partner,
                        'mess_time': mess_time,
                    }
                    mess_data.append(sub_dict)

        return {
            'mess_data': mess_data,
        }
    except Exception as e:
        logging.getLogger("error_logger").error("load_inbox. " + repr(e))


@user_login_required
def read(request, sub_id):
    try:
        context = load_read(request, sub_id)
        if request.is_ajax():
            return JsonResponse(context)
        else:
            return render(request, 'read.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("read. " + repr(e))
        messages.error(request, "Can't read!")
        return render(request, 'read.html')


def load_read(request, sub_id):
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        all_mess = get_all_messages_of_subject(sub_id)
        mess_dict = []
        for mess in all_mess:
            if not mess.read and not request.is_ajax():
                mess.read = True
                mess.save()
            mess_dict.append(mess.dumps())
        chat_subject = ChatSubject.nodes.get(sub_id=sub_id)
        if user.username == mess_dict[0]['sender_username']:
            partner = mess_dict[0]['receiver_username']
        else:
            partner = mess_dict[0]['sender_username']

        return {
            'mess_dict': mess_dict,
            'chat_subject': chat_subject.dumps(),
            'user': user.dumps(),
            'partner': partner
        }
    except Exception as e:
        logging.getLogger("error_logger").error("read. " + repr(e))


@user_login_required
def compose(request):
    return render(request, 'compose.html')


@user_login_required
def compose_to(request, username):
    try:
        context = {
            'username_to': username
        }
        return render(request, 'compose.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("read. " + repr(e))
        messages.error(request, "Can't read!")
        return render(request, 'compose.html')


@user_login_required
def send(request):
    if request.method == "POST":
        try:
            send_subject(request)
            return HttpResponseRedirect(reverse("mas:inbox"))

        except Exception as e:
            logging.getLogger("error_logger").error("send. " + repr(e))
            return HttpResponseRedirect(reverse("mas:compose"))

    else:
        return HttpResponseRedirect(reverse("mas:compose"))


def send_subject(request):
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)

        receiver_username = request.POST.get('receiver_username').strip()

        if receiver_username == "" or receiver_username == user.username:
            messages.warning(request, "Receiver has invalid username!")
            raise Exception

        receiver = get_user_by_username(receiver_username)
        if receiver is None:
            messages.warning(request, "Don't has receiver named by " + receiver_username + "!")
            raise Exception

        subject = request.POST.get('subject').strip()

        first_message = request.POST.get('first_message').strip()
        first_message = first_message.replace("\r\n", "<br>")
        if not first_message or first_message == "":
            messages.warning(request, "Please type something in message field!")
            raise Exception

        chat_subject = ChatSubject(
            subject=subject,
            creator_username=user.username,
            create_date=datetime.now()
        ).save()
        user.subject_send.connect(chat_subject)
        chat_subject.receiver.connect(receiver)

        info = get_info_of_user(user)
        sender_name = ""
        if info.first_name is not None and info.last_name is not None:
            sender_name = info.first_name + " " + info.last_name

        chat_message = ChatMessage(
            sender_username=user.username,
            sender_name=sender_name,
            receiver_username=receiver_username,
            send_date=datetime.now(),
            message=first_message,
        ).save()
        chat_subject.chat_message.connect(chat_message)
        chat_subject.save()
    except Exception as e:
        logging.getLogger("error_logger").error("send_message. " + repr(e))
        raise


@user_login_required
def reply(request):
    if request.method == "POST":
        try:
            reply_message(request)

        except Exception as e:
            logging.getLogger("error_logger").error("reply. " + repr(e))

        return JsonResponse({})
    return JsonResponse({})


def reply_message(request):
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)

        partner_username = request.POST.get('partner_username').strip()
        sub_id = request.POST.get('sub_id').strip()
        mess_content = request.POST.get('mess_content').strip()
        mess_content = mess_content.replace("\r\n", "<br>")

        read_all(user.username, sub_id)

        chat_subject = ChatSubject.nodes.get(sub_id=sub_id)

        info = get_info_of_user(user)
        sender_name = ""
        if info.first_name is not None and info.last_name is not None:
            sender_name = info.first_name + " " + info.last_name

        chat_message = ChatMessage(
            sender_username=user.username,
            sender_name=sender_name,
            receiver_username=partner_username,
            send_date=datetime.now(),
            message=mess_content,
        ).save()
        chat_subject.chat_message.connect(chat_message)
        chat_subject.save()
    except Exception as e:
        logging.getLogger("error_logger").error("reply_message. " + repr(e))
        return None
