from django.db import models

from dashboard.models import get_info_of_user
from dreamers_mas.models import *


def get_all_subjects(user):
    try:
        query = "MATCH(u: User {username: '" + user.username + \
                "'})--(c:ChatSubject) RETURN c ORDER BY c.create_date DESC"
        results, meta = db.cypher_query(query)
        return [ChatSubject.inflate(row[0]) for row in results]
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_subjects: " + repr(e))
        return None


def get_all_messages_of_subject(sub_id):
    try:
        query = "MATCH(s: ChatSubject {sub_id: '" + sub_id + \
                "'})--(m:ChatMessage) RETURN m ORDER BY m.send_date"
        results, meta = db.cypher_query(query)
        return [ChatMessage.inflate(row[0]) for row in results]
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_subjects: " + repr(e))
        return []

def read_all(username, sub_id):
    try:
        query = "MATCH(s: ChatSubject {sub_id: '" + sub_id + "'})--(m:ChatMessage) WHERE m.receiver_username = " \
                                                             "'" + username + "'AND m.read = false RETURN m"
        results, meta = db.cypher_query(query)
        for c in [ChatMessage.inflate(row[0]) for row in results]:
            c.read = True
            c.save()
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_subjects: " + repr(e))


def count_messages_of_subject(subject):
    try:
        query = "MATCH(s: ChatSubject {sub_id: '" + subject.sub_id + \
                "'})--(m:ChatMessage) RETURN count(m)"
        results, meta = db.cypher_query(query)
        return results[0][0]
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_subjects: " + repr(e))
        return 0


def get_all_supporters():
    try:
        query = "match(u:User{user_type:0}) return u"
        results, meta = db.cypher_query(query)
        supporters = []
        for user in [User.inflate(row[0]) for row in results]:
            supporters.append({
                "supporter": user,
                "name": get_supporter_name(user)
            })
        return supporters
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_supporters: " + repr(e))
        return []


def get_supporter_name(user):
    try:
        info = get_info_of_user(user)
        return info.first_name + " " + info.last_name
    except Exception as e:
        logging.getLogger("error_logger").error("get_supporter_name: " + repr(e))
        return ""
