from django.urls import path
from . import views

urlpatterns = [
    path('', views.inbox, name='inbox'),
    path('read/<sub_id>/', views.read, name='read'),
    path('compose/', views.compose, name='compose'),
    path('compose-to/<username>/', views.compose_to, name='compose_to'),

    path('send/', views.send, name='send'),
    path('reply/', views.reply, name='reply'),
]