import logging
from datetime import datetime

from campaign.models import get_all_campaigns_of_user, get_last_payment_of_user
from dreamers_mas.decorators import user_login_required
from homepage.models import get_user_by_uid
from support.models import get_all_supporters


def add_variable_to_context(request):
    try:
        uid = request.session['uid']
        if uid is not None:
            user = get_user_by_uid(uid)
            cams = get_all_campaigns_of_user(user)
            supporters = get_all_supporters()
            return {
                'list_campaigns': cams,
                'user': user,
                'supporters': supporters
            }
    except Exception as e:
        # logging.getLogger('error').error('add_variable_to_context', repr(e))
        return {}
