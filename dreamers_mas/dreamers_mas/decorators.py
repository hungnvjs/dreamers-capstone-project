import logging
from datetime import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from campaign.models import get_last_payment_of_user
from homepage.models import get_user_by_uid


def user_login_required(f):
    def wrap(request, *args, **kwargs):
        # this check the session if uid key exist, if not it will redirect to homepage
        try:
            if 'uid' not in request.session.keys():
                return HttpResponseRedirect(reverse('homepage:index'))
            else:
                uid = request.session['uid']
                user = get_user_by_uid(uid)
                if user.user_type != 1 or user.is_active is False:
                    return HttpResponseRedirect(reverse('homepage:index'))

            return f(request, *args, **kwargs)
        except Exception as e:
            logging.getLogger("error_logger").error("user_login_required. " + repr(e))
            return HttpResponseRedirect(reverse('homepage:index'))

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def admin_login_required(f):
    def wrap(request, *args, **kwargs):
        try:
            # this check the session if uid key exist, if not it will redirect to homepage
            if 'uid' not in request.session.keys():
                return HttpResponseRedirect(reverse('homepage:index'))
            else:
                uid = request.session['uid']
                user = get_user_by_uid(uid)
                if user.user_type != 0 or user.is_active is False:
                    return HttpResponseRedirect(reverse('homepage:index'))

            return f(request, *args, **kwargs)
        except Exception as e:
            logging.getLogger("error_logger").error("admin_login_required. " + repr(e))
            return render(request, 'index.html')

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def login_required(f):
    def wrap(request, *args, **kwargs):
        try:
            # this check the session if uid key exist, if not it will redirect to homepage
            if 'uid' not in request.session.keys():
                return HttpResponseRedirect(reverse('homepage:index'))
            # else:
            #     uid = request.session['uid']
            #     user = get_user_by_uid(uid)
            #     if user.user_type != 2:
            #         return HttpResponseRedirect(reverse('homepage:index'))
            return f(request, *args, **kwargs)

        except Exception as e:
            logging.getLogger("error_logger").error("login_required. " + repr(e))
            return render(request, 'index.html')

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap
