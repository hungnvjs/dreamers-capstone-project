from datetime import datetime

from neomodel import (StructuredNode, StringProperty, IntegerProperty,
                      UniqueIdProperty, RelationshipTo, RelationshipFrom, EmailProperty, FloatProperty, BooleanProperty,
                      DateTimeProperty)
from neomodel.relationship import *
from django.db import models
from passlib.handlers.pbkdf2 import pbkdf2_sha256
from uuid import uuid1
import logging


class Payment(StructuredRel):
    uid = StringProperty()
    pid = StringProperty()
    price = FloatProperty()
    start_date = DateTimeProperty()
    end_date = DateTimeProperty()

    # --status
    # 0: Available
    # 1: Expire
    status = IntegerProperty(default=0)


class User(StructuredNode):
    uid = StringProperty(unique_index=True)
    username = StringProperty(unique_index=True)
    password = StringProperty()
    is_active = BooleanProperty(default=False)

    # --user_type
    # 0: Admin
    # 1: End User
    user_type = IntegerProperty(default=2)

    def dumps(self):
        return {
            'uid': self.uid,
            'username': self.username,
            'password': self.password,
            'is_active': self.is_active,
            'user_type': self.user_type
        }

    def save_without_encrypt(self, *args, **kwargs):  # Code for save User without encrypt password
        # self.password = pbkdf2_sha256.encrypt(self.password, rounds=12000, salt_size=32)
        self.username = self.username.lower()
        super(User, self).save(*args, **kwargs)
        return self

    def save(self, *args, **kwargs):  # Code for encrypt Password
        self.username = self.username.lower()
        self.password = pbkdf2_sha256.encrypt(self.password, rounds=12000, salt_size=32)
        super(User, self).save(*args, **kwargs)
        return self

    def verify_password(self, raw_password):  # Code for verify Password
        return pbkdf2_sha256.verify(raw_password, self.password)

    # relationship
    info = RelationshipTo("Information", "HAS")
    package = RelationshipTo("Package", "USES", model=Payment)
    campaign = RelationshipTo("Campaign", "OWNS")
    subject_send = RelationshipTo("ChatSubject", "SENDS")
    subject_receive = RelationshipFrom("ChatSubject", "RECEIVES")


class Information(StructuredNode):
    first_name = StringProperty(default="")
    last_name = StringProperty(default="")
    company_name = StringProperty(default="")
    email = EmailProperty(unique_index=True)
    phone = StringProperty(default="")

    # relationship
    user = RelationshipFrom(User, "HAS")



class Campaign(StructuredNode):
    cam_id = StringProperty()  # not unique_index=True because cam_id may be duplicate with other User
    cam_name = StringProperty()
    start_date = DateTimeProperty()
    end_date = DateTimeProperty()
    # --status
    # 0: Enable
    # 1: Disable
    # 2: Campaign is created but not already to run
    status = IntegerProperty(default=0)

    # relationship
    uid = RelationshipFrom(User, "OWNS")
    pid = RelationshipTo("Package", "BELONGS_TO")
    channel_id = RelationshipTo("Channel", "USES")
    clothes = RelationshipTo("Clothes", "HAS")
    consumer = RelationshipTo("Consumer", "HAS")
    report = RelationshipTo("Report", "HAS")

    def get_clothes_by_id(self, clo_id):
        results, columns = self.cypher("match(n:Clothes {clo_id:'{clo_id}'})--(m:{self}) return n")
        return [self.inflate(row[0]) for row in results]


class Package(StructuredNode):
    # PKG1: Trial
    # PKG2: Standard
    # PKG3: Premium
    pid = StringProperty(unique_index=True)
    pname = StringProperty(unique_index=True)
    current_price = FloatProperty()
    duration = IntegerProperty()
    max_campaign = IntegerProperty()
    max_consumer = IntegerProperty()
    max_clothes = IntegerProperty()
    max_purchase_history = IntegerProperty()

    # relationship
    owner = RelationshipFrom(User, "USES", model=Payment)
    channel = RelationshipTo("Channel", "CONTAINS")
    cam = RelationshipFrom(Campaign, "BELONGS_TO")


class Channel(StructuredNode):
    channel_id = StringProperty(unique_index=True)
    channel_name = StringProperty(unique_index=True)

    # relationship
    package = RelationshipFrom(Package, "CONTAINS")
    cam = RelationshipFrom(Campaign, "USES")


class PurchaseHistory(StructuredRel):
    con_id = StringProperty()
    clo_id = StringProperty()
    purchase_time = DateTimeProperty()
    quantity = IntegerProperty()
    price = FloatProperty()

    def __str__(self):
        return self.con_id + " | " + self.clo_id + " | " + self.purchase_time.strftime(
            "%Y-%m-%d %H:%M") + " | " + self.quantity + " | " + self.price


class Clothes(StructuredNode):
    clo_id = StringProperty()
    name = StringProperty()

    # --age range 13 - 26
    age_range = StringProperty()
    season = StringProperty()
    material = StringProperty()
    color = StringProperty()
    price = FloatProperty()
    style = StringProperty()

    # --gender
    # 0: Male
    # 1: Female
    gender = IntegerProperty()
    brand = StringProperty()

    # --clo_type
    # 0: Áo
    # 1: Quần
    clo_type = IntegerProperty()

    # relationship
    campaign = RelationshipFrom(Campaign, "HAS")
    consumer = RelationshipFrom("Consumer", "BUYS", model=PurchaseHistory)


class Consumer(StructuredNode):
    con_id = StringProperty()
    name = StringProperty(default="")
    age = IntegerProperty()

    # --gender
    # 0: Male
    # 1: Female
    gender = IntegerProperty()
    email = EmailProperty(default="")
    phone_number = StringProperty(default="")
    facebook_id = StringProperty(default="")
    zalo_id = StringProperty(default="")
    address = StringProperty(default="")

    # relationship
    campaign = RelationshipFrom(Campaign, "HAS")
    clothes = RelationshipTo("Clothes", "BUYS", model=PurchaseHistory)


class OurStory(StructuredNode):
    description = StringProperty()


class OurService(StructuredNode):
    title = StringProperty()
    description = StringProperty()


class IMAS(StructuredNode):
    title = StringProperty()
    short_description = StringProperty()
    description = StringProperty()


class OurPricing(StructuredNode):
    short_description = StringProperty()
    description = StringProperty()


class OurTeam(StructuredNode):
    short_description = StringProperty()


class OurContact(StructuredNode):
    short_description = StringProperty()
    address = StringProperty()
    phone_number = StringProperty()
    email = StringProperty()


class ChatSubject(StructuredNode):
    sub_id = UniqueIdProperty()
    subject = StringProperty()
    creator_username = StringProperty()
    create_date = DateTimeProperty()

    # relationship
    chat_message = RelationshipTo('ChatMessage', "CONTAINS")
    sender = RelationshipFrom(User, "SENDS")
    receiver = RelationshipTo(User, "RECEIVES")

    def dumps(self):
        return {
            'sub_id': self.sub_id,
            'subject': self.subject,
            'creator_username': self.creator_username,
            'create_date': self.create_date.strftime("%Y-%m-%d %H:%M")
        }


class ChatMessage(StructuredNode):
    mess_id = UniqueIdProperty()
    sender_username = StringProperty()
    sender_name = StringProperty()
    receiver_username = StringProperty()
    send_date = DateTimeProperty()
    message = StringProperty()

    # False: not yet
    # True: read
    read = BooleanProperty(default=False)

    # relationship
    chat_subject = RelationshipFrom(ChatSubject, "CONTAINS")

    def dumps(self):
        return {
            'mess_id': self.mess_id,
            'sender_username': self.sender_username,
            'sender_name': self.sender_name,
            'receiver_username': self.receiver_username,
            'send_date': self.send_date.strftime("%Y-%m-%d %H:%M"),
            'message': self.message,
            'read': self.read
        }


class Report(StructuredNode):
    rpt_id = UniqueIdProperty()
    create_date = DateTimeProperty()
    num_recommend = IntegerProperty()
    precision = FloatProperty()
    recall = FloatProperty()

    # relationship
    campaign = RelationshipFrom(Campaign, "HAS")
