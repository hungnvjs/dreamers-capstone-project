import csv
import logging
import threading
import time
from threading import Thread

from django.core.files.uploadedfile import InMemoryUploadedFile

from campaign.models import *
from imas import imas
from datetime import datetime
from .models import *
from .channels import *
import random


class RunIMAS(Thread):
    def __init__(self, user, campaign):
        Thread.__init__(self)
        self.user = user
        self.campaign = campaign

    def run(self):
        start_time = datetime.now()

        print("Start AI: " + self.user.uid + self.campaign.cam_id)
        imas_output = imas.do_imas_algorithms(self.user, self.campaign)
        print("End AI: " + self.user.uid + self.campaign.cam_id)
        end_time = datetime.now()

        # get report

        print('Run time of AI:')
        imas_run_time = round((end_time - start_time).seconds, 2)
        print(imas_run_time)

        print('recommendations:')
        recommendations = imas_output['recommendations']
        print(recommendations)

        print('metrics:')
        metrics = imas_output['metrics']
        print(metrics)

        # set schedule
        try:
            client = facebook_client()
            for key, value in recommendations.items():
                con_id = key
                rec = value
                t = Thread(target=self.send_recommendations,
                           args=(self.user, self.campaign, con_id, rec['time'], rec['items'], client))
                t.start()
        except Exception as fbe:
            print(fbe)

        for key, value in metrics.items():
            create_date = key
            mets = value
            report = Report(create_date=create_date,
                            num_recommend=mets[1],
                            precision=mets[0][0],
                            recall=mets[0][1]
                            ).save()
            # ing
            report.campaign.connect(self.campaign)

    def send_recommendations(self, user, campaign, con_id, recommend_time, recommend_items, facebook_client):
        # calculate sleep time for first send
        recommend_time = datetime.strptime(recommend_time, '%H:%M')
        current_time = datetime.strptime(datetime.strftime(datetime.now(), '%H:%M'), '%H:%M')
        one_day = 86400
        sleep_time = 1

        duration = abs((recommend_time - current_time).total_seconds())
        current_second = current_time.second

        if current_time < recommend_time:
            sleep_time = current_second + one_day + duration
        if current_time > recommend_time:
            sleep_time = current_second + one_day - duration

        # recommendation scheduled list
        info = get_info_of_user(user)
        consumer = get_consumer_of_campaign(con_id, campaign)
        clothes_list = []
        for item in recommend_items:
            clothes_list.append(get_clothes_of_campaign(item, campaign))

        sleep_time = 2
        sleep_times = [sleep_time, one_day * 3, one_day * 3, one_day * 3, one_day * 3]

        count = 0

        for st in sleep_times:
            time.sleep(st)
            if len(clothes_list) < 6 and count >= len(clothes_list):
                rand = random.randint(0, len(clothes_list) - 1)

                # send mess
                style = str(clothes_list[rand].style).replace('_', ' ').capitalize()
                content_messenger = "Xin chào " + consumer.name + "\n\n Đến lúc làm mới tủ đồ của mình rồi đấy!\n\n" + \
                                    "Thời gian gần đây có rất nhiều sản phẩm mới đã lên kệ ở " + info.company_name + ". " \
                                                                                                                     "Nếu bạn đang phân vân với quá nhiều lựa chọn thì chúng tôi nghĩ rằng sản phẩm dưới đây sẽ phù hợp với bạn:\n\n" + \
                                    "Tên: " + clothes_list[rand].name + "\nHãng: " + clothes_list[rand].brand + \
                                    "\nGiá: " + str(clothes_list[rand].price) + " VNĐ " + "\nMàu:" + str(
                    clothes_list[rand].color) + \
                                    "\nStyle: " + style + \
                                    "\n\n" + "Nếu bạn quan tâm, hãy liên hệ với " + info.company_name + " để được phục vụ tốt nhất nhé!" + "\nLiên hệ: \nPhone: 0" + info.phone + \
                                    "\nEmail: " + info.email

                messenger = IMASMessenger(facebook_client)
                messenger.send(consumer.facebook_id, content_messenger)

                # send email
                if consumer.gender == 0:
                    gender = 'Nam'
                else:
                    gender = 'Nữ'

                content_email = render_to_string('recommendation_email_template.html', {
                    'consumer': consumer, 'clothes': clothes_list[rand],
                    'info': info, 'gender': gender, 'style': style,
                })
                mail_subject = '[' + info.company_name + '] Product Recommendation for ' + consumer.name
                to_email = consumer.email

                email = IMASEmail()
                email.send(to_email, mail_subject, content_email)
                count += 1

            else:
                style = str(clothes_list[count].style).replace('_', ' ').capitalize()
                content_messenger = "Xin chào " + consumer.name + "\n\n Đến lúc làm mới tủ đồ của mình rồi đấy!\n\n" + \
                                    "Thời gian gần đây có rất nhiều sản phẩm mới đã lên kệ ở " + info.company_name + "." \
                                                                                                                     "Nếu bạn đang phân vân với quá nhiều lựa chọn thì chúng tôi nghĩ rằng sản phẩm dưới đây sẽ phù hợp với bạn:\n\n" + \
                                    "Tên: " + clothes_list[count].name + "\nHãng: " + clothes_list[count].brand + \
                                    "\nGiá: " + str(clothes_list[count].price) + " VNĐ " + "\nMàu:" + str(
                    clothes_list[count].color) + \
                                    "\nStyle: " + style + \
                                    "\n\n" + "Nếu bạn quan tâm, hãy liên hệ với " + info.company_name + " để được phục vụ tốt nhất nhé!" + "\nLiên hệ: \nPhone: 0" + info.phone + \
                                    "\nEmail: " + info.email

                messenger = IMASMessenger(facebook_client)
                messenger.send(consumer.facebook_id, content_messenger)

                # send email
                if consumer.gender == 0:
                    gender = 'Nam'
                else:
                    gender = 'Nữ'
                content_email = render_to_string('recommendation_email_template.html', {
                    'consumer': consumer, 'clothes': clothes_list[count],
                    'info': info, 'gender': gender, 'style': style,
                })
                mail_subject = '[' + info.company_name + '] Product Recommendation for ' + consumer.name
                to_email = consumer.email

                email = IMASEmail()
                email.send(to_email, mail_subject, content_email)

                count += 1


class UpdateCampaignThread(Thread):
    def __init__(self, name, request, user, campaign, new_doc):
        Thread.__init__(self)
        self.name = name
        self.request = request
        self.user = user
        self.campaign = campaign
        self.new_doc = new_doc

    def run(self):
        # print("Thread: " + self.name)

        # get package user used
        package = get_current_package_of_user(self.user)
        self.campaign.status = 2

        # create consumers, clothes and connect campaign to
        if self.new_doc.con_file is not None:
            create_consumers = CreateConsumersThread(self.new_doc.con_file, self.user.uid, self.campaign, package)
            create_consumers.start()
            create_consumers.join()

        if self.new_doc.clo_file is not None:
            create_clothes = CreateClothesThread(self.new_doc.clo_file, self.user.uid, self.campaign, package)
            create_clothes.start()
            create_clothes.join()

        if self.new_doc.pur_file is not None:
            create_purchases = CreatePurchasesThread(self.new_doc.pur_file, self.user.uid, self.campaign, package)
            create_purchases.start()
            create_purchases.join()

        # print("End Thread: " + self.name)
        self.campaign.status = 0
        self.campaign.save()

        iMAS = RunIMAS(self.user, self.campaign)
        iMAS.start()

        # return HttpResponseRedirect(reverse("mas:list_campaign"))


threadLock = threading.Lock()


class CreateConsumersThread(Thread):
    def __init__(self, consumers_csv, uid, campaign, package):
        Thread.__init__(self)
        self.consumers_csv = consumers_csv
        self.uid = uid
        self.campaign = campaign
        self.package = package

    def run(self):
        print("Thread: " + self.name)
        # Get lock to synchronize threads
        threadLock.acquire()
        remain_num_consumer = self.package.max_consumer - count_all_clothes_of_campaign(self.uid, self.campaign.cam_id)
        try:

            decoded_file = self.consumers_csv.read().decode('utf-8').splitlines()
            reader = csv.DictReader(decoded_file)

            # loop over the lines and save them in db. If error , store as string and then display
            for index, row in zip(range(remain_num_consumer),
                                  reader):  # limit number of consumer when create each campaign
                try:
                    new_con = get_consumer_of_campaign(row["con_id"], self.campaign)
                    if new_con is None:
                        # create consumer
                        new_con = Consumer.create(row)[0]

                        # connect campaign to consumer
                        new_con.campaign.connect(self.campaign)
                    else:
                        new_con.name = row["name"]
                        new_con.age = int(row["age"])
                        new_con.gender = int(row["gender"])
                        new_con.email = row["email"]
                        new_con.phone_number = row["phone_number"]
                        new_con.facebook_id = row["facebook_id"]
                        new_con.zalo_id = row["zalo_id"]
                        new_con.address = row["address"]
                        new_con.save()

                except Exception as e:
                    logging.getLogger("error_logger").error(repr(e))
                    pass

            if not isinstance(self.consumers_csv, InMemoryUploadedFile):
                self.consumers_csv.delete()
        except Exception as e:
            logging.getLogger('error_logger').error("create_consumers. " + repr(e))
            pass

        # Free lock to release next thread
        threadLock.release()


class CreateClothesThread(Thread):
    def __init__(self, products_csv, uid, campaign, package):
        Thread.__init__(self)
        self.products_csv = products_csv
        self.uid = uid
        self.campaign = campaign
        self.package = package

    def run(self):
        # print("Thread: " + self.name)
        # Get lock to synchronize threads
        threadLock.acquire()
        remain_num_clothes = self.package.max_clothes - count_all_clothes_of_campaign(self.uid, self.campaign.cam_id)

        try:
            decoded_file = self.products_csv.read().decode('utf-8').splitlines()
            reader = csv.DictReader(decoded_file)

            # loop over the lines and save them in db. If error , store as string and then display
            for index, row in zip(range(remain_num_clothes),
                                  reader):  # limit number of clothes when create each campaign
                try:
                    new_clo = get_clothes_of_campaign(row["clo_id"], self.campaign)
                    if new_clo is None:
                        # create clothes
                        new_clo = Clothes.create(row)[0]

                        # connect campaign to clothes
                        new_clo.campaign.connect(self.campaign)
                    else:
                        new_clo.name = row["name"]
                        new_clo.age_range = row["age_range"]
                        new_clo.season = row["season"]
                        new_clo.material = row["material"]
                        new_clo.color = row["color"]
                        new_clo.price = float(row["price"])
                        new_clo.style = row["style"]
                        new_clo.gender = int(row["gender"])
                        new_clo.brand = row["brand"]
                        new_clo.clo_type = int(row["clo_type"])

                except Exception as e:
                    logging.getLogger("error_logger").error(repr(e))
                    pass

            if not isinstance(self.products_csv, InMemoryUploadedFile):
                self.products_csv.delete()
        except Exception as e:
            logging.getLogger('error_logger').error("create_clothes. " + repr(e))
            pass

        # Free lock to release next thread
        threadLock.release()


class CreatePurchasesThread(Thread):
    def __init__(self, purchases_csv, uid, campaign, package):
        Thread.__init__(self)
        self.purchases_csv = purchases_csv
        self.uid = uid
        self.campaign = campaign
        self.package = package

    def run(self):
        print("Thread: " + self.name)
        # Get lock to synchronize threads
        threadLock.acquire()
        try:
            remain_num_purchase = self.package.max_purchase_history - count_all_purchases_of_campaign(self.uid,
                                                                                                      self.campaign.cam_id)

            decoded_file = self.purchases_csv.read().decode('utf-8').splitlines()
            reader = csv.DictReader(decoded_file)

            # loop over the lines and save them in db. If error , store as string and then display
            for index, row in zip(range(remain_num_purchase),
                                  reader):  # limit number of purchase history when create each campaign

                try:
                    con_id = row["con_id"]
                    clo_id = row["clo_id"]
                    purchase_time = datetime.strptime(row["purchase_time"], "%Y-%m-%d %H:%M")
                    con = get_consumer_of_campaign(con_id, self.campaign)
                    clo = get_clothes_of_campaign(clo_id, self.campaign)
                    if con is not None and clo is not None:
                        pur_rel = get_rel_consumer_buy_clothes(con, clo, purchase_time)
                        if pur_rel is None:
                            pur_rel = con.clothes.connect(clo, {
                                'con_id': row["con_id"],
                                'clo_id': row["clo_id"],
                                'purchase_time': purchase_time,
                                'quantity': row["quantity"],
                                'price': row["price"]
                            })
                            pur_rel.save()
                        else:
                            pur_rel.quantity = row["quantity"]
                            pur_rel.price = row["price"]
                            pur_rel.save()

                except Exception as e:
                    logging.getLogger("error_logger").error(repr(e))
                    pass

            if not isinstance(self.purchases_csv, InMemoryUploadedFile):
                self.purchases_csv.delete()

        except Exception as e:
            logging.getLogger('error_logger').error("create_purchases. " + repr(e))
            pass

        # Free lock to release next thread
        threadLock.release()
