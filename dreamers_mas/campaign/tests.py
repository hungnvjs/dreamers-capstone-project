from django.test import TestCase
from .models import Campaign
from .views import *


# Create your tests here.
class CampaignTests(TestCase):
    def setUp(self):
        Campaign(
            cam_id='CAM100',
            cam_name='Sell some clothes',
            start_date='2018-02-15 9:00',
            end_date='2018-05-15 9:00',
            status=0
        )

    def test_list_campaign_path(self):
        response = self.client.get('/mas/campaign/')
        self.assertEqual(response.status_code, 200)

    def test_list_campaign_template(self):
        response = self.client.get('/mas/campaign/')
        self.assertTemplateUsed(response, 'manage-campaign.html')
    #
    # def test_go_create_campaign_path(self):
    #     response = self.client.get('/mas/campaign/create-campaign/')
    #     self.assertEqual(response.status_code, 200)

    def test_go_create_campaign_template(self):
        response = self.client.get('/mas/campaign/create-campaign/')
        self.assertTemplateUsed(response, 'create-campaign.html')



