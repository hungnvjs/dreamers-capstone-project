from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_campaign, name='list_campaign'),
    path('create-campaign/', views.go_create_campaign, name='go_create_campaign'),
    path('create-campaign/create/', views.create_campaign, name='create_campaign'),
    path('pause/<cam_id>/', views.pause_campaign, name='pause_campaign'),
    path('resume/<cam_id>/', views.resume_campaign, name='resume_campaign'),

    path('go-update/<cam_id>/', views.go_update_campaign, name='go_update_campaign'),
    path('update/<cam_id>/', views.update_campaign, name='update_campaign'),

    path('delete/<cam_id>/', views.delete_campaign, name='delete_campaign'),
    path('download-template/', views.download_template, name='download_template'),

    path('update-creating-campaign-data/', views.update_creating_campaign_data, name='update_creating_campaign_data'),

]
