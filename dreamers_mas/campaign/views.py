import os
import threading
from datetime import datetime
from json import JSONEncoder

from neomodel import OUTGOING, Traversal, db

from campaign.threads import *
from dashboard.views import check_expire
from dreamers_mas import settings
from dreamers_mas.decorators import user_login_required
from homepage.models import get_user_by_uid
from .models import *
from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse
import logging, csv


@user_login_required
def list_campaign(request):
    check_expire(request)
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)

        cams = get_all_campaigns_of_user(user)
        if check_remain_number_campaign(user) < 1:
            show_create_btn = False
        else:
            show_create_btn = True

        pay = get_last_payment_of_user(user)

        # check expire
        expire = True
        if pay is not None:
            end = pay.end_date.strftime("%Y-%m-%d %H:%M")
            now = datetime.now().strftime("%Y-%m-%d %H:%M")
            remain_time = (datetime.strptime(end, "%Y-%m-%d %H:%M") - datetime.strptime(now,
                                                                                        "%Y-%m-%d %H:%M")).total_seconds()
            if remain_time > 0:
                expire = False
        else:
            expire = True

        package = get_current_package_of_user(user)
        cams_data_show = []
        if cams is not None:
            for cam in cams:
                cams_data_show.append({
                    'cam': cam,
                    'num_cons': count_all_consumers_of_campaign(uid, cam.cam_id),
                    'num_clos': count_all_clothes_of_campaign(uid, cam.cam_id),
                    'num_purs': count_all_purchases_of_campaign(uid, cam.cam_id)
                })
        context = {
            'package': package,
            'expire': expire,
            'cams_data_show': cams_data_show,
            'show_create_btn': show_create_btn,
        }
        return render(request, 'manage-campaign.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("list_campaign. " + repr(e))
        messages.error(request, "Can't list campaigns!")
        return render(request, 'manage-campaign.html')


@user_login_required
def go_create_campaign(request):
    check_expire(request)
    return render(request, 'create-campaign.html')


def check_remain_number_campaign(user):
    # check max campaign then get current number of campaign
    list_cam = get_all_campaigns_of_user(user)
    if list_cam is not None:
        num_cam = len(list_cam)
    else:
        num_cam = 0

    # get package user used
    package = get_current_package_of_user(user)
    if package is None:
        pkg_max_cam = 0
    else:
        pkg_max_cam = package.max_campaign
    return pkg_max_cam - num_cam


def valid_csv_files(request, csv_files):
    try:
        file_dict = {}
        for csv_file in csv_files:  # check all files
            # print(csv_file.name)
            if not csv_file.name.endswith('.csv'):
                raise Exception('File ' + csv_file.name + ' is not CSV type !')
            # if file is too large, return
            if csv_file.multiple_chunks():
                raise Exception("Uploaded " + csv_file.name + " file is too big (%.2f MB)." % (
                    csv_file.size / (1000 * 1000),))

            file_dict[csv_file.name] = csv_file

        # check all file name
        try:
            if file_dict["consumers.csv"] and file_dict["products.csv"] and file_dict["purchase_histories.csv"] \
                    and len(file_dict) == 3:
                return file_dict
        except KeyError:
            raise Exception("3 file's name must be consumers.csv, products.csv, purchase_histories.csv !")
    except Exception as e:
        logging.getLogger("error_logger").error("valid_csv_files. " + repr(e))
        raise


# @user_login_required
def update_creating_campaign_data(request):
    try:
        if request.is_ajax():
            uid = request.session['uid']
            user = get_user_by_uid(uid)

            context = {
                'live': False
            }

            for th in creating_campaign_threads:
                # get campaign is creating
                if th.is_alive():
                    if th.user.uid == uid and th.campaign.status == 2:
                        cam_id = th.campaign.cam_id
                        package = get_current_package_of_user(user)
                        context = {
                            'live': True,
                            'num_cons': count_all_consumers_of_campaign(uid, cam_id),
                            'num_clos': count_all_clothes_of_campaign(uid, cam_id),
                            'num_purs': count_all_purchases_of_campaign(uid, cam_id),
                            'num_cons_max': package.max_consumer,
                            'num_clos_max': package.max_clothes,
                            'num_purs_max': package.max_purchase_history
                        }
                        break
            return JsonResponse(context)
        else:
            return HttpResponseRedirect(reverse("mas:list_campaign"))
    except Exception as e:
        logging.getLogger("error_logger").error("update_creating_campaign_data. " + repr(e))
        return HttpResponseRedirect(reverse('mas:list_campaign'))


creating_campaign_threads = []

@user_login_required
# @db.transaction
def create_campaign(request):
    global creating_campaign_threads
    check_expire(request)
    uid = request.session['uid']
    user = get_user_by_uid(uid)
    try:

        if count_number_campaign_creating(uid) > 0:
            messages.warning(request, "Your are creating a campaign, let's wait to it's completed!")
            raise Exception("You can't create many campaign at the one times!")

        if check_remain_number_campaign(user) < 1:
            messages.warning(request, "You have created maximum number of campaign !")
            raise Exception("Number of campaign was maximum !")

        # Only proceed with method POST
        if request.method == "POST":
            try:
                # csv = request.FILES["csv_file"]  # get 1 file
                csv_files = request.FILES.getlist("csv_file")  # get file list
                file_dict = valid_csv_files(request, csv_files)
                cam_name = request.POST.get('cam_name').strip()

                if not check_campaign_name(request, user, cam_name):
                    return HttpResponseRedirect(reverse("mas:go_create_campaign"))

                # get payment to take end date
                pay = get_last_payment_of_user(user)

                cam = Campaign(
                    cam_id=generate_cam_id(user.uid),
                    cam_name=cam_name,
                    start_date=datetime.now(),
                    end_date=pay.end_date,
                    status=2
                ).save()

                # get package user used
                package = get_current_package_of_user(user)

                new_doc = None
                try:
                    new_doc = CreateCampaignFiles(
                        con_file=file_dict["consumers.csv"],
                        clo_file=file_dict["products.csv"],
                        pur_file=file_dict["purchase_histories.csv"],
                    )
                    new_doc.save()
                except Exception as e:
                    pass
                # create consumers, clothes and connect campaign to
                create_cam_thread = UpdateCampaignThread(request, uid + "" + cam.cam_id, user, cam, new_doc)
                create_cam_thread.start()
                creating_campaign_threads.append(create_cam_thread)

                # connect campaign to user, package, channel
                user.campaign.connect(cam)
                cam.pid.connect(package)

                # travel all channels of package to campaign connect to
                definition = dict(
                    node_class=Channel,
                    direction=OUTGOING,
                    relation_type=None,
                    model=None
                )
                relations_channel_traversal = Traversal(package, Channel.__label__, definition)
                all_channel_relations = relations_channel_traversal.all()
                for channel in all_channel_relations:
                    cam.channel_id.connect(channel)
                cam.save()

                messages.success(request, "Campaign \' " + cam.cam_name + " \' is creating!")

            except Exception as e:
                logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
                messages.error(request, "Unable to upload file.")
                return HttpResponseRedirect(reverse("mas:go_create_campaign"))
            return HttpResponseRedirect(reverse("mas:list_campaign"))
        else:
            return render(request, "create-campaign.html")
    except Exception as e:
        logging.getLogger("error_logger").error("create_campaign " + repr(e))
        messages.error(request, "Can't create campaign!")
        return HttpResponseRedirect(reverse("mas:list_campaign"))


@user_login_required
def pause_campaign(request, cam_id):
    check_expire(request)
    if request.method == "GET":
        uid = request.session['uid']
        cam = get_campaign_of_user(cam_id, uid)
        if cam is not None and cam.status == 0:
            cam.status = 1  # 1 is disable
            cam.save()
            messages.success(request, 'Paused campaign \'' + cam.cam_name + '\'!')
    return HttpResponseRedirect(reverse("mas:list_campaign"))


@user_login_required
@db.transaction
def resume_campaign(request, cam_id):
    check_expire(request)
    if request.method == "GET":
        uid = request.session['uid']
        cam = get_campaign_of_user(cam_id, uid)
        if cam is not None and cam.status == 1:
            cam.status = 0  # 0 is enable
            cam.save()
            messages.success(request, 'Resumed campaign \'' + cam.cam_name + '\'!')
    return HttpResponseRedirect(reverse("mas:list_campaign"))


@user_login_required
def go_update_campaign(request, cam_id):
    check_expire(request)
    uid = request.session['uid']
    user = get_user_by_uid(uid)
    if request.method == "GET":
        cam = get_campaign_of_user(cam_id, user.uid)
        if cam is not None and cam.status == 0:
            context = {
                'cam_id': cam.cam_id,
                'cam_name': cam.cam_name
            }
            return render(request, 'update-campaign.html', context)
    else:
        return HttpResponseRedirect(reverse("mas:list_campaign"))


def valid_csv_file(request, csv_file, require_name):
    try:
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File ' + csv_file.name + ' is not CSV type!')
            raise Exception
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Uploaded " + csv_file.name + " file is too big (%.2f MB)." % (
                csv_file.size / (1000 * 1000),))
            raise Exception

        if csv_file.name != require_name:
            messages.error(request, "Your " + csv_file.name + " must be named by '" + require_name + "'!")
            raise Exception

        return True
    except Exception as e:
        logging.getLogger("error_logger").error("valid_csv_file. " + repr(e))
        raise


def check_campaign_name(request, user, cam_name):
    if cam_name == "":
        messages.warning(request, "Your campaign's name mustn't be empty!")
        return False
    if len(cam_name) > 25:
        messages.warning(request, "Your campaign's name mustn't be less than 25 characters!")
        return False
    exist = check_campaign_name_exist(user, cam_name)
    if exist is None:
        messages.error(request, "Your campaign's name has problem!")
        return False
    elif exist:
        messages.warning(request, "Your campaign's name is existed! Please create other name.")
        return False
    return True


@user_login_required
def update_campaign(request, cam_id):
    check_expire(request)
    # Only proceed with method POST
    if request.method == "POST":
        try:
            uid = request.session['uid']
            user = get_user_by_uid(uid)
            if count_number_campaign_creating(uid) > 0:
                messages.warning(request, "Your are updating a campaign, let's wait to it's completed!")
                raise Exception("You can't update many campaign at the one times!")

            cam = get_campaign_of_user(cam_id, user.uid)
            new_cam_name = request.POST.get('cam_name').strip()
            if new_cam_name != cam.cam_name and not check_campaign_name(request, user, new_cam_name):
                return HttpResponseRedirect(reverse("mas:go_update_campaign"))

            # get files
            consumers_files = request.FILES.getlist("consumers_file")
            products_files = request.FILES.getlist("products_file")
            purchases_files = request.FILES.getlist("purchases_file")

            num_files = len(consumers_files) + len(products_files) + len(purchases_files)
            # create more consumers, clothes and connect campaign to
            if num_files == 0 and new_cam_name == cam.cam_name:
                messages.warning(request, "Campaign " + cam.cam_name + " has not updated!")
                return HttpResponseRedirect(reverse("mas:go_update_campaign", args=(cam_id,)))
            else:
                if num_files != 0:
                    cam.status = 2
                    cam.save()

                    new_doc = CreateCampaignFiles()
                    if len(consumers_files) == 1:
                        if valid_csv_file(request, consumers_files[0], "consumers.csv"):
                            try:
                                new_doc.con_file = consumers_files[0]
                                new_doc.save()
                            except Exception as e:
                                pass
                        else:
                            messages.error(request, "Can't update consumer(s) in campaign " + cam.cam_name + "!")
                            return HttpResponseRedirect(reverse("mas:go_update_campaign", args=(cam_id,)))

                    if len(products_files) == 1:
                        if valid_csv_file(request, products_files[0], "products.csv"):
                            try:
                                new_doc.clo_file = products_files[0]
                                new_doc.save()
                            except Exception as e:
                                pass
                        else:
                            messages.error(request, "Can't update product(s) in campaign " + cam.cam_name + "!")
                            return HttpResponseRedirect(reverse("mas:go_update_campaign", args=(cam_id,)))

                    if len(purchases_files) == 1:
                        if valid_csv_file(request, purchases_files[0], "purchase_histories.csv"):
                            try:
                                new_doc.pur_file = purchases_files[0]
                                new_doc.save()
                            except Exception as e:
                                pass
                        else:
                            messages.error(request, "Can't update purchase history in campaign " + cam.cam_name + "!")
                            return HttpResponseRedirect(reverse("mas:go_update_campaign", args=(cam_id,)))

                    update_cam_thread = UpdateCampaignThread(request, uid + "" + cam.cam_id, user, cam, new_doc)
                    update_cam_thread.start()
                    creating_campaign_threads.append(update_cam_thread)

                else:
                    messages.warning(request, "No data update of " + cam.cam_name + "!")

                if new_cam_name != cam.cam_name:
                    old_name = cam.cam_name
                    cam.cam_name = new_cam_name
                    cam.save()
                    messages.success(request,
                                     "Campaign " + old_name + " has been changed name to " + new_cam_name + "!")
                cam.save()
            return HttpResponseRedirect(reverse("mas:list_campaign"))

        except Exception as e:
            logging.getLogger("error_logger").error("update_campaign. " + repr(e))
            messages.error(request, "Can't update campaign " + cam.cam_name + "!")
            return HttpResponseRedirect(reverse("mas:go_update_campaign", args=(cam_id,)))

    else:
        return HttpResponseRedirect(reverse("mas:go_update_campaign", args=(cam_id,)))


def send_message_info_create(request, type, data_name, info, campaign):
    if type == "update":
        if info['num_created'] > 0:
            messages.success(request, "Campaign " + campaign.cam_name + " has " + str(info['num_created'])
                             + " " + data_name + " were added new!")

        if info['num_updated'] > 0:
            messages.warning(request, "Campaign " + campaign.cam_name + " has " + str(info['num_updated'])
                             + " " + data_name + " were duplicated and updated!")

    if info['num_can_not_create'] > 0:
        messages.error(request, "Campaign " + campaign.cam_name + " has " + str(info['num_can_not_create'])
                       + " " + data_name + " can't add because invalid fields!")


@user_login_required
@db.transaction
def delete_campaign(request, cam_id):
    check_expire(request)
    if request.method == "GET":
        uid = request.session['uid']
        cam = get_campaign_of_user(cam_id, uid)
        if cam.status < 2:
            del_all_consumers_of_campaign(cam)
            del_all_clothes_of_campaign(cam)
            cam.delete()
            messages.success(request, 'Deleted campaign \'' + cam.cam_name + '\'!')
        else:
            messages.success(request, 'Can\'t delete campaign \'' + cam.cam_name + '\' because it\'s creating!')
    return HttpResponseRedirect(reverse("mas:list_campaign"))


@user_login_required
def download_template(request):
    file_path = os.path.join(settings.MEDIA_ROOT, "documents/template_file/templates.v1.0.zip")
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404
