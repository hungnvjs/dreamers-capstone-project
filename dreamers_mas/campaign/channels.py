from django.template.loader import render_to_string
from fbchat import Client, Message, ThreadType
from django.core.mail import EmailMessage
import logging


def facebook_client():

    try:
        print('Logging on account 1')
        user1 = '100028032256906'
        password1 = 'Abc@12345'
        client1 = Client(user1, password1)
        return client1
    except:
        try:
            print('Logging on account 2')
            user2 = '100020707193522'
            password2 = 'tinhte123'
            client2 = Client(user2, password2)
            return client2
        except:
            try:
                print('Logging on account 3')
                user3 = '100028304652298'
                password3 = 'Abc@12345'
                client3 = Client(user3, password3)
                return client3
            except:
                try:
                    print('Logging on account 4')
                    user4 = '100028278613927'
                    password4 = 'Abc@12345'
                    client4 = Client(user4, password4)
                    return client4
                except:
                    print('Have no account available')





# facebook messenger
class IMASMessenger():
    def __init__(self, client):
        self.client = client

    def send(self, facebook_link, content):
        try:
            # receiver = self.client.searchForUsers(username)
            user_id = facebook_link.split('com/')[1]
            self.client.send(Message(text=content), thread_id=user_id, thread_type=ThreadType.USER)
            status = 'SEND_DONE'

        except Exception as e:
            logging.getLogger("error_logger").error("facebook_messenger_send_error. " + repr(e))
            status = 'CAN_NOT_SEND'

        return status


# email
class IMASEmail():
    def send(self, to_email, mail_subject, content):
        try:
            email = EmailMessage(mail_subject, content, to=[to_email])
            email.send()
            status = 'SEND_DONE'
        except Exception as e:
            logging.getLogger("error_logger").error("email_send_error. " + repr(e))
            status = 'CAN_NOT_SEND'
        return status


