from dreamers_mas.models import *
from neomodel import OUTGOING, INCOMING, EITHER, Traversal, db
import logging


class CreateCampaignFiles(models.Model):
    con_file = models.FileField(upload_to='documents/temp')
    clo_file = models.FileField(upload_to='documents/temp')
    pur_file = models.FileField(upload_to='documents/temp')


def get_consumer_of_campaign(con_id, campaign):
    try:
        consumes = campaign.consumer.search(con_id=con_id)
        if len(consumes) > 0:
            return consumes[0]
        return None
    except Exception as e:
        # logging.getLogger("error_logger").error("get_consumer_of_campaign: " + repr(e))
        return None


def get_clothes_of_campaign(clo_id, campaign):
    try:
        return campaign.clothes.search(clo_id=clo_id)[0]
    except Exception as e:
        # logging.getLogger("error_logger").error("get_clothes_of_campaign. " + repr(e))
        return None


def get_rel_consumer_buy_clothes(consumer, clothes, purchase_time):
    try:
        pur_list = consumer.clothes.all_relationships(clothes)
        for pur in pur_list:
            if purchase_time.strftime("%Y-%m-%d %H:%M") == pur.purchase_time.strftime("%Y-%m-%d %H:%M"):
                return pur
        return None
    except Exception as e:
        logging.getLogger("error_logger").error("get_rel_consumer_buy_clothes. " + repr(e))
        return None


# def get_clothes_by_id(clo_id, cam_id):
#     try:
#         query = "match(n:Clothes {clo_id:'" + str(clo_id) + "'})--(m:Campaign {cam_id:'" + str(cam_id) + "'}) return n"
#         results, meta = db.cypher_query(query)
#         return [Clothes.inflate(row[0]) for row in results][0]
#     except Exception as e:
#         logging.getLogger("error_logger").error("Get clothes by ID. " + repr(e))
#         return None


def del_all_consumers_of_campaign(campaign):
    for con in campaign.consumer.all():
        con.delete()


def del_all_clothes_of_campaign(campaign):
    for clo in campaign.clothes.all():
        clo.delete()


# auto create with max number of id code in database
def generate_cam_id(uid):
    try:
        query = "MATCH (c:Campaign)--(u:User { uid: '" + uid + "' }) RETURN c.cam_id"
        results, meta = db.cypher_query(query)
        cam_id_number_list = []
        for n in results:
            x = (int(n[0][3:]))
            # print(x)
            cam_id_number_list.append(x)
        new_cam_id = "CAM0"
        if len(cam_id_number_list) > 0:
            cam_id_number_list.sort()
            max_cam_code = cam_id_number_list[len(cam_id_number_list) - 1]
            new_cam_id = "CAM" + str(max_cam_code + 1)

        return new_cam_id
    except Exception as e:
        logging.getLogger("error_logger").error("Get cam max ID. " + repr(e))
        return None


def get_current_package_of_user(user):
    try:
        payment = get_last_payment_of_user(user)
        return user.package.match(start_date=payment.start_date)[0]
    except Exception as e:
        # logging.getLogger("error_logger").error("get_current_package_of_user: " + repr(e))
        return None


def get_last_payment_of_user(user):
    try:
        query = "MATCH(u: User {uid: '" + user.uid + "'})" \
                                                     "-[r: USES]->(p:Package) RETURN r ORDER BY r.start_date DESC LIMIT 1"
        results, meta = db.cypher_query(query)
        return [Payment.inflate(row[0]) for row in results][0]
    except Exception as e:
        # logging.getLogger("error_logger").error("get_last_payment_of_user: " + repr(e))
        return None


def get_all_campaigns_of_user(user):
    try:
        return user.campaign.all()
    except Exception as e:
        # logging.getLogger("error_logger").error("get_all_campaign_by_uid: " + repr(e))
        raise


def get_all_consumers_of_campaign(campaign):
    try:
        cons = campaign.consumer.all()
        if cons is None:
            return []
        return cons
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_campaign_by_uid: " + repr(e))
        raise


def count_all_consumers_of_campaign(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--(con:Consumer) return count(con)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("count_all_consumers_of_campaign: " + repr(e))
        raise


def get_all_clothes_of_campaign(campaign):
    try:
        clos = campaign.clothes.all()
        if clos is None:
            return []
        return clos
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_clothes_of_campaign: " + repr(e))
        raise


def count_all_clothes_of_campaign(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--(clo:Clothes) return count(clo)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("count_all_clothes_of_campaign: " + repr(e))
        raise


def get_all_purchases_of_campaign(user, campaign):
    try:
        query = "match(u:User{uid:'" + user.uid + "'})--(c:Campaign{cam_id:'" + campaign.cam_id + "'})--(con:Consumer)-[r:BUYS]->(clo:Clothes) return r"
        results, meta = db.cypher_query(query)
        purs = [PurchaseHistory.inflate(row[0]) for row in results]
        if purs is None:
            return []
        return purs
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_purchases_of_campaign: " + repr(e))
        raise


def count_all_purchases_of_campaign(uid, cam_id):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign{cam_id:'" + cam_id + "'})--(con:Consumer)-[r:BUYS]->(clo:Clothes) return count(r)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("count_all_purchases_of_campaign: " + repr(e))
        raise


def get_campaign_of_user(cam_id, uid):
    try:
        query = "MATCH(u: User {uid: '" + uid + "'})--(c:Campaign {cam_id: '" + cam_id + "'}) RETURN c"
        results, meta = db.cypher_query(query)
        return [Campaign.inflate(row[0]) for row in results][0]
    except Exception as e:
        logging.getLogger("error_logger").error("get_campaign_of_user: " + repr(e))
        return None


def check_campaign_name_exist(user, cam_name):
    try:
        cam = user.campaign.search(cam_name=cam_name)
        if len(cam) == 0:
            return False
        else:
            return True
    except Exception as e:
        logging.getLogger("error_logger").error("check_campaign_name_exist: " + repr(e))
        return None


def get_info_of_user(user):
    try:
        return user.info.all()[0]
    except Exception as e:
        logging.getLogger("error_logger").error("get_info_of_user: " + repr(e))
        return None


def count_number_campaign_creating(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign) where c.status = 2 return count(c)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("count_number_campaign_creating: " + repr(e))
        return 0
