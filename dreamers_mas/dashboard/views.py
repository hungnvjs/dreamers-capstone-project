from datetime import datetime, timedelta

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse
from django.shortcuts import render
from django.urls import reverse

from campaign.models import get_last_payment_of_user, get_all_campaigns_of_user
from homepage.views import validation
from .models import *
from dreamers_mas.decorators import user_login_required
from homepage.models import get_user_by_uid, logging
from .models import *


# Create your views here.
@user_login_required
def dashboard(request):
    try:
        check_expire(request)
        context = check_noti(request)

        uid = request.session['uid']
        user = get_user_by_uid(uid)

        total_revenue = cal_total_revenue(uid)
        context["total_revenue"] = total_revenue

        total_sales = get_total_sales(uid)
        context["total_sales"] = total_sales

        avg_all_recall = get_average_all_recall(uid)
        context["avg_all_recall"] = avg_all_recall*100

        avg_all_precision = get_average_all_precision(uid)
        context["avg_all_precision"] = avg_all_precision*100

        dashboard_data = make_dashboard_chart_data(uid)
        context["dashboard_data"] = dashboard_data

        return render(request, 'dashboard.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("dashboard. " + repr(e))
        messages.error(request, "Can't show dashboard!")
        return render(request, 'dashboard.html')


def make_dashboard_chart_data(uid):
    report_data = cal_all_reports(uid)
    sale_data = get_sale_last_15days(uid)
    dashboard_data = []
    sale_date = ""
    for s in sale_data:
        sale_date = s["sale_date"]
        try:
            report = report_data[sale_date]
            if report is None:
                report = {
                    "total_recommend": 0,
                    "avg_precision": 0,
                    "avg_recall": 0
                }
        except Exception as e:
            report = {
                "total_recommend": 0,
                "avg_precision": 0,
                "avg_recall": 0
            }
            pass
        dashboard_data.append({
            "date": sale_date,
            "sale": s["num_sales"],
            "precision": round(report["avg_precision"]*100, 1),
            "recall": round(report["avg_recall"]*100, 1)
        })
    if len(report_data) > 0:
        next_date = (datetime.strptime(sale_date, '%Y-%m-%d') + timedelta(days=1)).strftime("%Y-%m-%d")
        dashboard_data.append({
            "date": next_date
        })

    return dashboard_data


@user_login_required
def load_unread_mail(request):
    try:
        if request.is_ajax():
            context = check_noti(request)
            return JsonResponse(context)
        else:
            return HttpResponseRedirect(reverse("mas:dashboard"))
    except Exception as e:
        logging.getLogger("error_logger").error("load_unread_mail. " + repr(e))


def check_noti(request):
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        unread_mess = get_messages_unread_of_user(user.username)
        list_unread = []
        # only show 5 first unread email newest
        for mess in unread_mess:
            sub_id = get_sub_id_by_mess_id(mess.mess_id)
            list_unread.append({
                "mess": mess.dumps(),
                "sub_id": sub_id
            })

        context = {
            'list_unread': list_unread,
            'num_new': len(unread_mess),
        }
        return context

    except Exception as e:
        logging.getLogger("error_logger").error("check_noti. " + repr(e))
        messages.error(request, "Can't load mail!")


@user_login_required
def profile(request):
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        info = get_info_of_user(user)
        if info.first_name is None:
            info.first_name = ""
        if info.last_name is None:
            info.last_name = ""
        if info.company_name is None:
            info.company_name = ""
        if info.phone is None:
            info.phone = ""
        context = {
            'user': user,
            'info': info,
        }
        check_expire(request)
        return render(request, 'profile.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("profile. " + repr(e))
        messages.error(request, "Can't show your profile!")
        return render(request, 'dashboard.html')


def check_expire(request):
    try:
        uid = request.session['uid']
        if uid is not None:
            user = get_user_by_uid(uid)
            pay = get_last_payment_of_user(user)
            expire = False
            if pay is not None:
                end = pay.end_date.strftime("%Y-%m-%d %H:%M")
                now = datetime.now().strftime("%Y-%m-%d %H:%M")
                remain_time = (datetime.strptime(end, "%Y-%m-%d %H:%M") -
                               datetime.strptime(now, "%Y-%m-%d %H:%M")).total_seconds()
                if remain_time < 0:
                    # disable payment
                    pay.status = 1
                    pay.save()

                    # disable all campaigns
                    cams = get_all_campaigns_of_user(user)
                    if cams is not None:
                        for cam in cams:
                            cam.status = 1
                            cam.save()

                    expire = True
            return expire
    except Exception as e:
        # logging.getLogger('error').error('check_expire', repr(e))
        return {}


def check_old_pass(request):
    data = {}
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        uid = request.session['uid']
        if uid is not None:
            user = get_user_by_uid(uid)
            input_old_pass = request.POST.get('old_pass')
            if user is not None:
                if input_old_pass is None:
                    data['message'] = 'The old password must be filled'
                else:
                    if pbkdf2_sha256.verify(input_old_pass, user.password):
                        data['message'] = 'success'
                    else:
                        data['message'] = 'The Old Password is wrong!!'

            return JsonResponse(data)
        return HttpResponseRedirect(reverse('homepage:index'))


def profile_change_pass(request):
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        uid = request.session['uid']
        if uid is not None:
            user = get_user_by_uid(uid)
            input_old_pass = request.POST.get('old_password')
            new_pass = request.POST.get('new_password')
            re_new_pass = request.POST.get('re_new_password')
            if user is not None:
                if pbkdf2_sha256.verify(input_old_pass, user.password) and new_pass == re_new_pass and \
                        validation.is_valid_password(new_pass):
                    user.password = new_pass
                    user.save()
                    messages.success(request, 'Change password success')
                    return HttpResponseRedirect(reverse('mas:profile'))
        else:
            return HttpResponseRedirect(reverse('homepage:index'))


def profile_update_infor(request):#Update profile
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        uid = request.session['uid']
        if uid is not None:
            user = get_user_by_uid(uid)
            first_name = request.POST.get('first_name').strip()
            last_name = request.POST.get('last_name').strip()
            company_name = request.POST.get('company_name').strip()
            phone = request.POST.get('phone').strip()

            if user is not None:
                infor = get_info_of_user(user)
                infor.first_name = first_name
                infor.last_name = last_name
                infor.company_name = company_name
                if validation.is_valid_phone(phone):#validate Phonenumber
                    infor.phone = phone
                    infor.save()
                messages.success(request, 'Your information is updated')#popup message
                return HttpResponseRedirect(reverse('mas:profile'))
            else:
                return HttpResponseRedirect(reverse('homepage:index'))
        else:
            return HttpResponseRedirect(reverse('homepage:index'))

def check_phone(request):#check phone number to send to ajax
    data = {}
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        uid = request.session['uid']
        if uid is not None:
            user = get_user_by_uid(uid)
            phone = request.POST.get('phone')
            if user is not None:
                if validation.is_valid_phone(phone):
                    data['message'] = 'success'
                else:
                    data['message'] = 'Phone number is invalid!'
                return JsonResponse(data)
        else:
            return HttpResponseRedirect(reverse('homepage:index'))
