from dreamers_mas.models import *
import logging


def get_info_of_user(user):
    try:
        return user.info.all()[0]
    except Exception as e:
        logging.getLogger("error_logger").error("get_info_of_user: " + repr(e))
        return None


def get_messages_unread_of_user(username):
    try:
        query = "match(u:User{username:'" + username + "'})--(cs:ChatSubject)--(cm:ChatMessage) " \
                                                       "where cm.read = false and u.username = cm.receiver_username" \
                                                       " return cm order by cm.send_date desc"
        results, meta = db.cypher_query(query)
        return [ChatMessage.inflate(row[0]) for row in results]
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_subjects: " + repr(e))
        return None


def get_sub_id_by_mess_id(mess_id):
    try:
        query = "match(cm:ChatMessage{mess_id:'" + mess_id + "'})--(cs:ChatSubject) return cs.sub_id"
        results, meta = db.cypher_query(query)
        return results[0][0]
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_subjects: " + repr(e))
        return None


def cal_total_revenue(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign)--" \
                     "(con:Consumer)-[r:BUYS]->(clo:Clothes) with r.quantity*r.price as re return sum(re)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("cal_total_revenue: " + repr(e))
        return 0


def get_total_sales(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign)--(con:Consumer)-[r:BUYS]->" \
                                             "(clo:Clothes) return sum(r.quantity)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("get_total_sales: " + repr(e))
        return 0


def get_average_all_recall(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign)--(r:Report) return avg(r.recall)"
        results, meta = db.cypher_query(query)
        avg = results[0][0]
        if avg is None:
            return 0
        return avg
    except Exception as e:
        logging.getLogger("error_logger").error("get_average_all_recall: " + repr(e))
        return 0


def get_average_all_precision(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign)--(r:Report) return avg(r.precision)"
        results, meta = db.cypher_query(query)
        avg = results[0][0]
        if avg is None:
            return 0
        return avg
    except Exception as e:
        logging.getLogger("error_logger").error("get_average_all_precision: " + repr(e))
        return 0


def cal_all_reports(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign)--(r:Report)" \
                                             " return r order by r.create_date desc"
        results, meta = db.cypher_query(query)
        reports = [Report.inflate(row[0]) for row in results]
        # count_day = 0
        count = 0
        report_data = {}
        if len(reports) == 0:
            return report_data
        create_date = reports[0].create_date.strftime("%Y-%m-%d")
        total_recommend = 0
        total_precision = 0
        total_recall = 0
        for r in reports:
            if r.create_date.strftime("%Y-%m-%d") == create_date:
                total_recommend += r.num_recommend
                total_precision += r.precision
                total_recall += r.recall
                count += 1
            else:
                report_data[create_date] = {
                    "total_recommend": total_recommend,
                    "avg_precision": total_precision/count,
                    "avg_recall": total_recall/count
                }
                total_recommend = r.num_recommend
                total_precision = r.precision
                total_recall = r.recall
                count = 1
                create_date = r.create_date.strftime("%Y-%m-%d")
            #     count_day += 1
            # if count_day > 15:
            #     break
        return report_data
    except Exception as e:
        logging.getLogger("error_logger").error("cal_all_reports: " + repr(e))
        return []


def get_sale_last_15days(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})--(c:Campaign)--" \
                                     "(con:Consumer)-[r:BUYS]->(clo:Clothes) return r order by r.purchase_time desc"
        results, meta = db.cypher_query(query)
        purs = [PurchaseHistory.inflate(row[0]) for row in results]
        count_day = 0
        sale_data = []
        sale_date = purs[0].purchase_time.strftime("%Y-%m-%d")
        num_sales = 0
        for p in purs:
            if p.purchase_time.strftime("%Y-%m-%d") == sale_date:
                num_sales += p.quantity
            else:
                sale_data.insert(0, {
                    "sale_date": sale_date,
                    "num_sales": num_sales
                })
                # sale_data[sale_date] = num_sales
                sale_date = p.purchase_time.strftime("%Y-%m-%d")
                num_sales = p.quantity
                count_day += 1
            if count_day > 15:
                break
        return sale_data
    except Exception as e:
        # logging.getLogger("error_logger").error("get_sale_last_15days: " + repr(e))
        return {}

