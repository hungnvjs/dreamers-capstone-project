from django import template

register = template.Library()

@register.filter
def nomal_mess(value):
    return value.replace("<br>", "\r\n")

@register.filter
def addstr(arg1, arg2):
    """concatenate arg1 & arg2"""
    return str(arg1) + str(arg2)
