from django.urls import path, include
from . import views

app_name = 'mas'
urlpatterns = [
    path('', views.dashboard, name='index'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('profile/', views.profile, name='profile'),
    path('profile/chang-password/', views.profile_change_pass, name='profile_change_pass'),
    path('profile/check-old-password/', views.check_old_pass, name='check_old_pass'),
    path('profile/update-infor/', views.profile_update_infor, name='profile_update_infor'),
    path('profile/check-phone/', views.check_phone, name='check_phone'),

    path('noti/', views.load_unread_mail, name='load_unread_mail'),


    path('report/', include('report.urls')),
    path('campaign/', include('campaign.urls')),
    path('support/', include('support.urls')),
    path('payment/', include('payment.urls')),
]
