function load_ai_chart(json) {
    var ai_chart = AmCharts.makeChart("ai_chart", {
        "type": "serial",
        "theme": "light",
        "dataDateFormat": "YYYY-MM-DD",
        "precision": 2,
        "valueAxes": [{
            "id": "v1",
            "title": "Sales",
            "position": "left",
            "autoGridCount": false,
            "labelFunction": function (value) {
                return Math.round(value) + " products";
            }
        }, {
            "id": "v2",
            "title": "Accuracy",
            "gridAlpha": 0,
            "position": "right",
            "autoGridCount": false,
            "labelFunction": function (value) {
                return Math.round(value) + "%";
            }
        }],
        "graphs": [{
            "id": "g3",
            "valueAxis": "v1",
            "lineColor": "#e1ede9",
            "fillColors": "#eda900",
            "fillAlphas": 1,
            "type": "column",
            "title": "Sales",
            "precision": 0,
            "valueField": "sale",
            "clustered": false,
            "columnWidth": 0.5,
            "legendValueText": "[[value]] p",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]] products</b>"
        }, {
            "id": "g4",
            "valueAxis": "v1",
            "lineColor": "#03A9F5",
            "fillColors": "#03A9F5",
            "fillAlphas": 1,
            "type": "column",
            "title": "Recommended",
            "precision": 0,
            "valueField": "recommend",
            "clustered": false,
            "columnWidth": 0.3,
            "legendValueText": "[[value]] p",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]] products</b>"
        }, {
            "id": "g1",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "lineColor": "#00ff01",
            "type": "smoothedLine",
            "title": "Recall",
            "useLineColorForBulletBorder": true,
            "valueField": "recall",
            "legendValueText": "[[value]] %",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]%</b>"
        }, {
            "id": "g2",
            "valueAxis": "v2",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "lineColor": "#E5343D",
            "type": "smoothedLine",
            "dashLength": 5,
            "title": "Precision",
            "useLineColorForBulletBorder": true,
            "valueField": "precision",
            "legendValueText": "[[value]] %",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]%</b>"
        }],
        "chartScrollbar": {
            "graph": "g1",
            "oppositeAxis": false,
            "offset": 30,
            "scrollbarHeight": 50,
            "backgroundAlpha": 0,
            "selectedBackgroundAlpha": 0.9,
            "selectedBackgroundColor": "#ffffff",
            "graphFillAlpha": 0,
            "graphLineAlpha": 0.5,
            "selectedGraphFillAlpha": 0,
            "selectedGraphLineAlpha": 1,
            "autoGridCount": true,
            "color": "#AAAAAA"
        },
        "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "cursorAlpha": 0,
            "valueLineAlpha": 0.2
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "legend": {
            "useGraphSettings": true,
            "position": "top"
        },
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
        },
        "export": {
            "enabled": true
        },
        "dataProvider": []
    });

    //Setting the new data to the graph
    ai_chart.dataProvider = json;

    //Updating the graph to show the new data
    ai_chart.validateData();
}

function load_doughnut_chart(channel_data) {
    <!--Doughnut echarts init-->

    var dom = document.getElementById("doughnut");
    var dnutChart = echarts.init(dom);

    var app = {};
    var values = Object.values(channel_data);
    option = null;
    option = {
        color: ['#62549a', '#4aa9e9', '#ff6c60', '#eac459', '#25c3b2'],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
            orient: 'vertical',
            x: 'left',
            data: ['Zalo', 'SMS', 'Email', 'Messenger']
        },
        calculable: true,
        series: [
            {
                name: 'Source',
                type: 'pie',
                radius: ['50%', '70%'],
                data: [
                    {value: values[0], name: 'Zalo'},
                    {value: values[1], name: 'SMS'},
                    {value: values[2], name: 'Email'},
                    {value: values[3], name: 'Messenger'}
                ]
            }
        ]
    };

    if (option && typeof option === "object") {
        dnutChart.setOption(option, false);
    }

}

function load_dashboard_chart(json) {

    var chartData = json;

    var chart = AmCharts.makeChart("dashboard_chart", {
        "type": "serial",
        "theme": "light",

        "dataDateFormat": "YYYY-MM-DD",
        "dataProvider": chartData,

        "addClassNames": true,
        "startDuration": 1,
        //"color": "#FFFFFF",
        "marginLeft": 0,

        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "minPeriod": "DD",
            "autoGridCount": false,
            "gridCount": 50,
            "gridAlpha": 0.1,
            "gridColor": "#FFFFFF",
            "axisColor": "#555555",
            "dateFormats": [{
                "period": 'DD',
                "format": 'DD'
            }, {
                "period": 'WW',
                "format": 'MMM DD'
            }, {
                "period": 'MM',
                "format": 'MMM'
            }, {
                "period": 'YYYY',
                "format": 'YYYY'
            }]
        },

        "valueAxes": [{
            "id": "a1",
            "title": "Sales",
            "gridAlpha": 0,
            "axisAlpha": 0
        }, {
            "id": "a2",
            "position": "right",
            "gridAlpha": 0,
            "axisAlpha": 0,
            "labelsEnabled": false
        }, {
            "id": "a3",
            "title": "Accuracy",
            "position": "right",
            "gridAlpha": 0,
            "axisAlpha": 0,
            "inside": true,
            "labelFunction": function (value) {
                return Math.round(value) + "%";
            }
        }],
        "graphs": [{
            "id": "g1",
            "valueField": "sale",
            "title": "Sales",
            "type": "column",
            "fillAlphas": 0.9,
            "valueAxis": "a1",
            "balloonText": "[[value]] products",
            "legendValueText": "[[value]] products",
            "legendPeriodValueText": "total: [[value.sum]] products",
            "lineColor": "#428bca",
            "alphaField": "alpha"
        }, {
            "id": "g2",
            "valueField": "precision",
            "classNameField": "bulletClass",
            "title": "Precision",
            "type": "line",
            "valueAxis": "a3",
            "lineColor": "#eda900",
            "lineThickness": 1,
            "legendValueText": "[[value]] %",
            "descriptionField": "townName",
            "bullet": "round",
            "bulletSizeField": "townSize",
            "bulletBorderColor": "#eda900",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 2,
            "bulletColor": "#eda900",
            "labelText": "[[townName2]]",
            "labelPosition": "right",
            "balloonText": "Precision: [[value]] %",
            "showBalloon": true,
            "animationPlayed": true
        }, {
            "id": "g3",
            "title": "Recall",
            "valueField": "recall",
            "type": "line",
            "valueAxis": "a3",
            "lineColor": "#E5343D",
            "balloonText": "Recall: [[value]] %",
            "lineThickness": 1,
            "legendValueText": "[[value]] %",
            "bullet": "square",
            "bulletBorderColor": "#E5343D",
            "bulletBorderThickness": 1,
            "bulletBorderAlpha": 1,
            "dashLengthField": "dashLength",
            "animationPlayed": true
        }],

        "chartCursor": {
            "zoomable": false,
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0,
            "valueBalloonsEnabled": false
        },
        "legend": {
            "bulletType": "round",
            "equalWidths": false,
            "valueWidth": 120,
            "useGraphSettings": true
            //"color": "#FFFFFF"
        }
    })
};
