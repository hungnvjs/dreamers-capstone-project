function createTable(file) {
    var table, tbody;
    if (file.name === "consumers.csv") {
        table = document.getElementById("tableConsumers");
        tbody = document.getElementById('tbodyConsumers');
    } else if (file.name === "products.csv") {
        table = document.getElementById("tableProducts");
        tbody = document.getElementById('tbodyProducts');
    } else if (file.name === "purchase_histories.csv") {
        table = document.getElementById("tablePurchases");
        tbody = document.getElementById('tbodyPurchases');
    }
    var reader = new FileReader();
    reader.onload = function (e) {
        var rowCount = table.rows.length;
        //remove all rows existed
        for (var x = rowCount - 1; x > 0; x--) {
            table.deleteRow(x);
        }
        // auto generate row table
        var rows = e.target.result.split("\n");
        for (var i = 1; i < rows.length && i < 6; i++) {
            var row = tbody.insertRow();
            var cells = rows[i].split(",");
            for (var j = 0; j < cells.length; j++) {
                var cell = row.insertCell(-1);
                cell.innerHTML = cells[j];
            }
        }
    }
    reader.readAsText(file);
}

function uploadCSV() {
    var csvFiles = document.getElementById("csvFileUpload");
    var filesLength = csvFiles.files.length;
    if (filesLength === 3) {
        for (var k = 0; k < filesLength; k++) {
            var file = csvFiles.files.item(k);
            var row_consumer = document.getElementById("row_consumers")
            row_consumer.classList.remove("d-none");
            var row_products = document.getElementById("row_products")
            row_products.classList.remove("d-none");
            var row_purchases = document.getElementById("row_purchases")
            row_purchases.classList.remove("d-none");
            createTable(file);
        }
        var spanFile = document.getElementById("spanfile");
        spanFile.innerHTML = "Completed!";
        spanFile.style.color = "#0fdac4";
        spanFile.style.borderColor = "#0fdac4";
        return true
    } else {
        notification_error("You have just selected " + filesLength + " file(s)!\nYou must to select 3 files (consumers.csv, products.csv, purchase_histories.csv)");
        return false
    }
}

function notification_error(message) {
    sweetAlert("Oops...", message, "error");
}

function notification_warning(message) {
    swal("Notice!", message)
}

function notification_success(message) {
    swal("Hey, Good job!", message, "success")
}

function check_update_campaign() {
    var csv_consumers = document.getElementById("consumers_file");
    var csv_products = document.getElementById("products_file");
    var csv_purchases = document.getElementById("purchases_file");
    var cam_name = document.getElementById("cam_name").value;
    var old_cam_name = document.getElementById("old_cam_name").value;
    var count = csv_consumers.files.length + csv_products.files.length + csv_purchases.files.length;
    if (count === 0 && old_cam_name === cam_name) {
        notification_error("You need to change campaign's name or select at least 1 file to update your campaign!");
        return false
    } else {
        return true
    }

}

function upload_consumers_update() {
    var csv_consumers = document.getElementById("consumers_file").files[0];
    if (csv_consumers.name === "consumers.csv") {
        var row_consumer = document.getElementById("row_consumers")
        row_consumer.classList.remove("d-none");
        createTable(csv_consumers);
        var spanFile = document.getElementById("span_consumers");
        spanFile.innerHTML = "Completed!";
        spanFile.style.color = "#0fdac4";
        spanFile.style.borderColor = "#0fdac4";
        return true
    } else {
        notification_error("You must select \"consumers.csv\" file !");
        return false
    }
}


function upload_products_update() {
    var csv_products = document.getElementById("products_file").files[0];
    if (csv_products.name === "products.csv") {
        var row_products = document.getElementById("row_products")
        row_products.classList.remove("d-none");
        createTable(csv_products);
        var spanFile = document.getElementById("span_products");
        spanFile.innerHTML = "Completed!";
        spanFile.style.color = "#0fdac4";
        spanFile.style.borderColor = "#0fdac4";
        return true
    } else {
        notification_error("You must select \"products.csv\" file !");
        return false
    }
}


function upload_purchases_update() {
    var csv_purchases = document.getElementById("purchases_file").files[0];
    if (csv_purchases.name === "purchase_histories.csv") {
        var row_purchases = document.getElementById("row_purchases")
        row_purchases.classList.remove("d-none");
        createTable(csv_purchases);
        var spanFile = document.getElementById("span_purchases");
        spanFile.innerHTML = "Completed!";
        spanFile.style.color = "#0fdac4";
        spanFile.style.borderColor = "#0fdac4";
        return true
    } else {
        notification_error("You must select \"purchase_histories.csv\" file !");
        return false
    }
}

function clearSelected() {
    document.getElementById("csvFileUpload").value = "";
    var spanFile = document.getElementById("spanfile");
    spanFile.innerHTML = "Choose files...";
    spanFile.style.color = "#626162";
    spanFile.style.borderColor = "lightgrey";
    clearTable("tableConsumers");
    clearTable("tableProducts");
    clearTable("tablePurchases");
}

function clear_consumers() {
    document.getElementById("consumers_file").value = "";
    var row_consumer = document.getElementById("row_consumers")
    row_consumer.classList.add("d-none");
    var spanFile = document.getElementById("span_consumers");
    spanFile.innerHTML = "Consumers";
    spanFile.style.color = "#626162";
    spanFile.style.borderColor = "lightgrey";
    clearTable("tableConsumers");
}

function clear_products() {
    document.getElementById("products_file").value = "";
    var row_consumer = document.getElementById("row_products")
    row_consumer.classList.add("d-none");
    var spanFile = document.getElementById("span_products");
    spanFile.innerHTML = "Products";
    spanFile.style.color = "#626162";
    spanFile.style.borderColor = "lightgrey";
    clearTable("tableProducts");
}

function clear_purchases() {
    document.getElementById("products_file").value = "";
    var row_consumer = document.getElementById("row_purchases")
    row_consumer.classList.add("d-none");
    var spanFile = document.getElementById("span_purchases");
    spanFile.innerHTML = "Purchase histories";
    spanFile.style.color = "#626162";
    spanFile.style.borderColor = "lightgrey";
    clearTable("tablePurchases");
}

function clearTable(tableName) {
    var table = document.getElementById(tableName);
    var rowCount = table.rows.length;
    //remove all rows existed
    for (var x = rowCount - 1; x > 0; x--) {
        table.deleteRow(x);
    }
}

function confirm_pause_campaign(cam_id) {
    swal({
            title: "Are you sure to pause this campaign?",
            text: "The iMAS will not run this campaign !!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ffb22b",
            confirmButtonText: "Yes, pause it !!",
            closeOnConfirm: true
        },
        function () {
            // swal("Deleted !!", "Hey, your imaginary file has been deleted !!", "success");
            location.href = "pause/" + cam_id;
            toastr_pause_success();
        });
};


function confirm_delete_campaign(cam_id) {
    swal({
            title: "Are you sure to delete this campaign?",
            text: "All data of this campaign will be deleted !!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ef5350",
            confirmButtonText: "Yes, delete it !!",
            closeOnConfirm: true
        },
        function () {
            // swal("Deleted !!", "Hey, your imaginary file has been deleted !!", "success");
            location.href = "delete/" + cam_id;
            toastr_pause_success();
        });
};


function confirm_delete_compose(back_link) {
    swal({
            title: "Are you sure to delete this compose?",
            text: "You will back to inbox page!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ef5350",
            confirmButtonText: "Yes, delete it !!",
            closeOnConfirm: true
        },
        function () {
            location.href = back_link;
        });
};


function confirm_renew_campaign(cam_id) {
    swal({
            title: "Are you sure to rdeleteenew this campaign?",
            text: "The iMAS will renew this campaign and execute with existed data before !!",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#1976d2",
            confirmButtonText: "Yes, renew it !!",
            closeOnConfirm: true
        },
        function () {
            // swal("Deleted !!", "Hey, your imaginary file has been deleted !!", "success");
            location.href = "renew/" + cam_id;
            toastr_pause_success();
        });
};

function confirm_pay_package(pid) {
    swal({
            title: "Your package will be update now!",
            text: "Are you sure to pay this package?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ffb22b",
            confirmButtonText: "Yes, pay for it!",
            closeOnConfirm: true
        },
        function () {
            // swal("Deleted !!", "Hey, your imaginary file has been deleted !!", "success");
            location.href = "pay-package/" + pid;
            // return true
        });
};

function toastr_success(message) {
    toastr.success(message, 'SUCCESS', {
        "positionClass": "toast-bottom-right",
        timeOut: 10000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false

    })
};

function toastr_warning(message) {
    toastr.warning(message, 'WARNING', {
        "positionClass": "toast-bottom-right",
        timeOut: 10000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false

    })
};

function toastr_error(message) {
    toastr.error(message, 'ERROR', {
        "positionClass": "toast-bottom-right",
        timeOut: 10000,
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false

    })
};

