var dom = document.getElementById("rainfall");
var rainChart = echarts.init(dom);

var app = {};
option = null;
option = {
    color: ['#4aa9e9', '#67f3e4', "#e90a00"],
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['Trial', 'Standard', 'Premium']
    },
    calculable: true,
    xAxis: [
        {
            type: 'category',
            data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        }
    ],
    yAxis: [
        {
            type: 'value'
        }
    ],
    series: [
        {
            name: 'Trial',
            type: 'bar',
            data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
            markPoint: {
                data: [
                    {type: 'max', name: 'Max'},
                    {type: 'min', name: 'Min'}
                ]
            },
            markLine: {
                data: [
                    {type: 'average', name: 'Average'}
                ]
            }
        },
        {
            name: 'Standard',
            type: 'bar',
            data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
            markPoint: {
                data: [
                    {name: 'Max', value: 182.2, xAxis: 7, yAxis: 183, symbolSize: 18},
                    {name: 'Min', value: 2.3, xAxis: 11, yAxis: 3}
                ]
            },
            markLine: {
                data: [
                    {type: 'average', name: 'Average'}
                ]
            }
        },
        {
            name: 'Premium',
            type: 'bar',
            data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
            markPoint: {
                data: [
                    {name: 'Max', value: 182.2, xAxis: 7, yAxis: 183, symbolSize: 18},
                    {name: 'Min', value: 2.3, xAxis: 11, yAxis: 3}
                ]
            },
            markLine: {
                data: [
                    {type: 'average', name: 'Average'}
                ]
            }
        }
    ]
};

if (option && typeof option === "object") {
    rainChart.setOption(option, false);
}



function load_doughnut_chart(package_json) {
    <!--Doughnut echarts init-->

    var dom = document.getElementById("package_chart");
    var dnutChart = echarts.init(dom);

    var app = {};
    var values = Object.values(package_json);
    option = null;
    option = {
        color: ['#62549a', '#4aa9e9', '#ff6c60', '#eac459', '#25c3b2'],
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
            orient: 'vertical',
            x: 'left',
            data: ['Trial', 'Standard', 'Premium']
        },
        calculable: true,
        series: [
            {
                name: 'Source',
                type: 'pie',
                radius: ['50%', '70%'],
                data: [
                    {value: values[0], name: 'Trial'},
                    {value: values[1], name: 'Standard'},
                    {value: values[2], name: 'Premium'}
                ]
            }
        ]
    };

    if (option && typeof option === "object") {
        dnutChart.setOption(option, false);
    }

}
