$(document).ready(function () {
    $("#btn-submit").on('click', function (event) { //Check username and password on Login
        event.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: "login/",
            data: $('#login_form').serialize(),
            success: function (resp) {
                if (resp['error_message'] == 'Success') {


                    location.reload();

                }
                else {
                    $("#error").fadeIn(1000, function () {
                        $("#error").html("<p class='error-message'>" + resp['error_message'] + "</p>");

                    })
                }
            },
            error: function () {
                alert('Error');
            }

        });
        return false;

    });


    $("#re_new_pass").on('change', function () { //compare pass1 and confirm password
        if ($("#new_pass").val() == $("#re_new_pass").val()) {
            $("#not_match").html("<p></p>");
            // {
            //     #$('input[id="submit_signup"]').attr("type", "submit");
            //     #
            // }
        } else
            $("#not_match").html("<p class='message'>Password is not Match</p>");
        // {
        //     #$('input[id="submit_signup"]').attr("type", "button");
        //     #
        // }
    });

    // $("#submit_signup").on('click', function () { //Check username and password on Login
    //
    //     if($('#old_pass[document_type]').val().length === 0)|| $('#new_pass[document_type]').val().length === 0) ){
    //         event.preventDefault();
    //
    //     }
    //     else
    //         return true;
    //
    //     return false;
    // });


});

function edit_package(event) {//set all field of package become input for changing
    // alert("Edit");
    swal({
            title: "Are you sure to edit this package?",
            text: "Information of this package will be change",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#efb11e",
            confirmButtonText: "Yes, Edit this package",
            closeOnConfirm: true
        },
        function () {
            currentRow = $(event).closest("tr");
            pgk_name = currentRow.find("td:eq(0)").text();
            pkg_campaign = currentRow.find("td:eq(1)");
            value_campaign = currentRow.find("td:eq(1)").text();
            pkg_consumer = currentRow.find("td:eq(2)");
            pkg_product = currentRow.find("td:eq(3)");
            pkg_purchase_history = currentRow.find("td:eq(4)");
            pkg_price = currentRow.find("td:eq(5)");
            pkg_duration = currentRow.find("td:eq(6)");
            submit_button = currentRow.find("td:eq(7)");

            // pgk_name.html("<input name='pkg_name' type='text'>");

            pkg_campaign.html("<input id='max_campaign' name='max_campaign' type='text' style='width: 80%;' value='"+value_campaign+"' required>");
            pkg_consumer.html("<input id='max_consumer' name='max_consumer' type='text' style='width: 80%;' value='"+pkg_consumer.text()+"' required>");
            pkg_product.html("<input id='max_product' name='max_product' type='text' style='width: 80%;' value='"+pkg_product.text()+"' required>");
            pkg_purchase_history.html("<input id='max_purchase_history' name='max_purchase_history' type='text' style='width: 80%;' value='"+pkg_purchase_history.text()+"' required>");
            pkg_price.html("<input id='current_price' name='current_price' type='text' style='width: 80%;' value='"+pkg_price.text()+"' required>");
            pkg_duration.html("<input id='duration' name ='max_duration'type='text' style='width: 80%;' value='"+pkg_duration.text()+"' required>");
            edit = submit_button.find("button:eq(0)");
            edit.hide();
            submit_button.append("<button type='button' id='btn_save' class='btn btn-success btn-outline' onclick='package_save(this)'>Save</button>");

        });


};


function package_save(event) {//save package after input the changes
    jname = pgk_name;
    jcam = $("#max_campaign").val();
    jcon = $("#max_consumer").val();
    jpro = $("#max_product").val();
    jpur = $("#max_purchase_history").val();
    jprice = $("#current_price").val();
    jduration = $("#duration").val();
    $.ajax({
        url: "/administrator/manage-package/edit/",
        data: {
            "csrfmiddlewaretoken": $("#btn_save").siblings("input[name='csrfmiddlewaretoken']").val(),
            "pgk_name": jname,
            "pkg_campaign": jcam,
            "pkg_consumer": jcon,
            "pkg_product": jpro,
            "pkg_purchase_history": jpur,
            "pkg_price": jprice,
            "pkg_duration": jduration

        },
        method: "POST",
        dataType: "json",
        success: function (returned_data) {
            if (returned_data['message'] == 'success') {

                edit.show();
                savebt = submit_button.find("button:eq(1)");
                savebt.hide();
                location.reload();
                // notification_success("Change Package's Information Success!!");
            } else {
                alert("Failed");
            }
        },
        error:function (data) {
            notification_error("All field must be digits!!");
        }
    });

};


function confirm_disable_user(element) {
    swal({
            title: "Are you sure to deactivate this User?",
            text: "This User will be deactivate and can not login!!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ef5350",
            confirmButtonText: "Yes, Deactivate this User!!",
            closeOnConfirm: true
        },
        function () {
            // alert("Button");
            currentRow = $(element).closest("tr");
            user_name = currentRow.find("td:eq(0)").text();
            change = currentRow.find("")
            // user_name = $("#user_name").val();
            // alert(user_name);
            is_active = false;
            $.ajax({
                url: $("#deactivate").attr("data-url"),
                data: {
                    "csrfmiddlewaretoken": $("#deactivate").siblings("input[name='csrfmiddlewaretoken']").val(),
                    "is_active": is_active,
                    "user_name": user_name
                },
                method: "POST",
                dataType: "json",
                success: function (returned_data) {
                    if (returned_data['message'] == 'success') {

                        // alert("change success");

                        // $("#deactivate").attr("class","fa fa-toggle-off tog-off");
                        change_button_to_deactivate(element);

                    } else {
                        alert("Failed");
                    }
                }
            });


        });


};

function change_button_to_deactivate(element) {

    $(element).attr("class", "fa fa-toggle-off tog-off");
    $(element).attr("data-url", "/administrator/manage-user/change-status-to-active/");
    $(element).attr("onclick", "confirm_enable_user(this)");
    $(element).attr("id", "active-user");
}

function confirm_enable_user(element) {
    swal({
            title: "Are you sure to active this User?",
            text: "This User will be active and can  login!!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ef5350",
            confirmButtonText: "Yes, Active this User!!",
            closeOnConfirm: true
        },
        function () {
            // alert("Button");
            currentRow = $(element).closest("tr");
            user_name = currentRow.find("td:eq(0)").text();
            change = currentRow.find("")
            // user_name = $("#user_name").val();
            // alert(user_name);
            is_active = true;
            $.ajax({
                url: $("#active-user").attr("data-url"),
                data: {
                    "csrfmiddlewaretoken": $("#active-user").siblings("input[name='csrfmiddlewaretoken']").val(),
                    "is_active": is_active,
                    "user_name": user_name,
                },
                method: "POST",
                dataType: "json",
                success: function (returned_data) {
                    if (returned_data['message'] == 'success') {

                        // alert("change success");

                        // $("#deactivate").attr("class","fa fa-toggle-off tog-off");
                        change_button_to_active(element);

                    } else {
                        alert("Failed");
                    }
                }
            });


        });


};



function change_button_to_active(element) {

    $(element).attr("class", "fa fa-toggle-on tog-on");
    $(element).attr("data-url", "/administrator/manage-user/change-status-to-deactivate/");
    $(element).attr("onclick", "confirm_disable_user(this)");
    $(element).attr("id", "deactivate");
}


function check_login(element) { //check username in register
    // {
    //     #$("#login_ok").hide();
    //     #
    // }
    $("#login_not").hide();

    signup = $(element).val();
    if (signup == "") {
        return;
    }
    $.ajax({
        url: $(element).attr("data-url"),
        data: {
            "csrfmiddlewaretoken": $(element).siblings("input[name='csrfmiddlewaretoken']").val(),
            "signup": signup
        },
        method: "POST",
        dataType: "json",
        success: function (returned_data) {
            if (returned_data.is_success) {
                // {
                //     #$("#login_ok").show();
                //     #
                // }
                $('input[id="submit_signup"]').attr("type", "submit");
            } else {
                $("#login_not").show();
                $('input[id="submit_signup"]').attr("type", "button");
            }
        }
    });
}

function clear_username() {
    // {
    //     #$("#login_ok").hide();
    //     #
    // }
    $("#login_not").hide();
}

function check_email(element) {//check e-mail from regex in view.py
    signup_email = $(element).val();
    if (signup_email == "") {
        return;
    }
    $.ajax({
        url: $(element).attr("data-url"),
        data: {
            "csrfmiddlewaretoken": $(element).siblings("input[name='csrfmiddlewaretoken']").val(),
            "signup_email": signup_email
        },
        method: "POST",
        dataType: "json",
        success: function (returned_data) {
            if (returned_data['is_success'] == 'success') {
                $('input[id="submit_signup"]').attr("type", "submit");
            } else {

                $("#email_not").html("<p class='message'>" + returned_data['is_success'] + "</p>");
                $('input[id="submit_signup"]').attr("type", "button");
            }
        }
    });
}

function check_old_pass(element) {//check old_password
    old_pass = $(element).val();
    if (old_pass == "") {
        return;
    }
    $.ajax({
        url: $(element).attr("data-url"),
        data: {
            "csrfmiddlewaretoken": $(element).siblings("input[name='csrfmiddlewaretoken']").val(),
            "old_pass": old_pass
        },
        method: "POST",
        dataType: "json",
        success: function (returned_data) {
            if (returned_data['message'] == 'success') {
                $('input[id="submit_signup"]').attr("type", "submit");
            } else {

                $("#error").html("<p class='message'>" + returned_data['message'] + "</p>");
                $('input[id="submit_signup"]').attr("type", "button");
            }
        }
    });
}


function clear_email() {
    $("#email_not").html("<p></p>");
}

function check_pass(element) {//Check password from regex in view.py

    signup_password = $(element).val();
    if (signup_password == "") {
        return;
    }
    $.ajax({
        url: $(element).attr("data-url"),
        data: {
            "csrfmiddlewaretoken": $(element).siblings("input[name='csrfmiddlewaretoken']").val(),
            "signup_password": signup_password
        },
        method: "POST",
        dataType: "json",
        success: function (data) {

            if (data['error_message'] == 'success') {
                // {
                //     #$("#email_ok").show();
                //     #
                // }
                $('input[id="submit_signup"]').attr("type", "submit");
            } else {

                $("#pass_wrong").html("<p class='message'>" + data['error_message'] + "</p>");
                $('input[id="submit_signup"]').attr("type", "button");
            }
        }
    });
}


function check_phone(element) {//Check password from regex in view.py
    phone = $(element).val();
    if (phone == "") {
        return;
    }
    $.ajax({
        url: $(element).attr("data-url"),
        data: {
            "csrfmiddlewaretoken": $(element).siblings("input[name='csrfmiddlewaretoken']").val(),
            "phone": phone,
        },
        method: "POST",
        dataType: "json",
        success: function (data) {

            if (data['message'] == 'success') {
                $("#pass_not").html("");
                $('input[id="submit_signup"]').attr("type", "submit");
            } else {

                $("#pass_not").html("<p class='message'>" + data['message'] + "</p>");
                $('input[id="submit_signup"]').attr("type", "button");
            }
        }
    });
}


// function clear_pass() {
//     $("#pass_not").html("<p></p>");
// }
//
// function clear_match() {
//     $("#not_match").html("<p></p>");
// }
//
// function clear_login() {
//     $("#error").html("<p></p>");
// }
