from campaign.models import *
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import datetime

# for run project
from .import database_context as data

# for run file in command line
# import database_context as data

DATA_CUTTING_MARK = 70


def building_binary_matrix_full(user, campaign):

    purchase_histories = data.get_purchase_histories_table(user,campaign)
    clothes = data.get_all_clothes_id_of_campaign(campaign)
    consumers = data.get_all_consumer_id_of_campaign(campaign)

    # null matrix with rows is consumer list, columns is product list
    binary_matrix = pd.DataFrame(index=consumers,columns=clothes)

    # Filling binary matrix with sorted consumer purchase history
    rows, columns = purchase_histories.shape
    for item in range(0, rows):
        row = purchase_histories.iloc[item][0]
        column = purchase_histories.iloc[item][1]
        binary_matrix[column][row] = 1

    return binary_matrix


def building_binary_matrix_recent_days(user, campaign):

    date = data.get_first_date_of_data_test(user, campaign)
    day_duration = data.get_recommend_available_duration()

    purchase_histories = data.get_purchase_histories_table_recent_days(user, campaign, date, day_duration)
    clothes = data.get_all_clothes_id_of_campaign(campaign)
    consumers = data.get_all_consumer_id_of_campaign(campaign)

    # null matrix with rows is consumer list, columns is product list
    binary_matrix = pd.DataFrame(index=consumers,columns=clothes)

    # Filling binary matrix with sorted consumer purchase history
    rows, columns = purchase_histories.shape
    for item in range(0, rows):
        row = purchase_histories.iloc[item][0]
        column = purchase_histories.iloc[item][1]
        binary_matrix[column][row] = 1

    return binary_matrix


def cosine_similarity_calculation(user, campaign):

    clothes = data.get_all_clothes_id_of_campaign(campaign)
    df = building_binary_matrix_full(user, campaign)
    df = df.fillna(0)
    correlation_matrix = cosine_similarity(df.transpose())

    df_correlation_matrix = pd.DataFrame(correlation_matrix, index=clothes, columns=clothes)

    return df_correlation_matrix


def cs_compare_model_with_history(user, campaign):

    # binary_matrix of recent days data
    binary_matrix = building_binary_matrix_recent_days(user, campaign)
    # matrix of full purchase data
    df_correlation_matrix = cosine_similarity_calculation(user, campaign)

    # storing list of output
    consumers = data.get_all_consumer_id_of_campaign(campaign)
    recommendation_output = {}
    recommendation_items = []
    for consumer in consumers:
        # take history of 1 specific consumer
        consumer_history = binary_matrix.loc[consumer, :]
        consumer_history = consumer_history.dropna()

        # find all product related to consumer history
        sim_candidates = pd.Series()
        for i in range(0, len(consumer_history.index)):
            # find all product related to 1 specific item
            sims = df_correlation_matrix[consumer_history.index[i]]
            sim_candidates = sim_candidates.append(sims)

        # sort list of related product in ascending order
        sim_candidates.sort_values(inplace=True, ascending=False)

        # remove duplicated values
        sim_candidates_removed_duplicated = sim_candidates[~sim_candidates.index.duplicated(keep='first')]
        # remove items that consumer bought before
        filtered_sims = sim_candidates_removed_duplicated.drop(consumer_history.index)
        # sort remaining items
        filtered_sims.sort_values(inplace=True, ascending=False)
        # remove item with value <= 0.4
        filtered_sims = filtered_sims[filtered_sims > 0.4]

        # store list of recommendation into dictionary: max 5
        recommendation_items = list(filtered_sims.index)
        if len(recommendation_items) > 5:
            recommendation_items = recommendation_items[:5]

        recommendation_time = data.get_recommend_hour_of_consumer(user, campaign, consumer)

        recommendation_output[consumer] = {'time': recommendation_time, 'items': recommendation_items}

    # calculate metrics
    date = data.get_first_date_of_data_test(user, campaign)
    data_test = data.cut_data(user, campaign, DATA_CUTTING_MARK, type='TEST')
    end_date = data_test[-1].purchase_time
    # not count 1 day before end_date
    loop_days = (end_date - date).days + 2
    print('loop days:' + str(loop_days))

    day_duration = data.get_recommend_available_duration()
    consumers = get_all_consumers_of_campaign(campaign)

    prf_all_day = {}

    date = date - datetime.timedelta(days=1)
    for i in range(loop_days):
        date = date + datetime.timedelta(days=1)
        prf_all_consumer_per_day = []
        for c in consumers:

            recommend_items = recommendation_items
            buy_items = data.get_bought_product_id_available_times_by_consumer(user, campaign, date, day_duration,
                                                                               c.con_id)
            recommend_success_items = []

            for ri in recommend_items:
                for bi in buy_items:
                    if ri == bi:
                        recommend_success_items.append(ri)
            recommend_success_items = list(set(recommend_success_items))

            # calculate
            if len(recommend_items) > 0 and len(buy_items) > 0:
                precision = len(recommend_success_items) / len(recommend_items)
                recall = len(recommend_success_items) / len(buy_items)
                f1 = 0
                if precision != 0 and recall != 0:
                    f1 = 2 / ((1 / precision) + (1 / recall))

                prf = [precision, recall, f1]
                prf_all_consumer_per_day.append(prf)

        prf_sum = [sum(i) for i in zip(*prf_all_consumer_per_day)]
        prf_average = None
        if len(prf_sum) != 0:
            precision_average = prf_sum[0] / len(prf_all_consumer_per_day)
            recall_average = prf_sum[1] / len(prf_all_consumer_per_day)
            f1_average = prf_sum[2] / len(prf_all_consumer_per_day)

            prf_average = [precision_average, recall_average, f1_average]

        if prf_average is not None:
            prf_all_day[date] = [prf_average, len(consumers)]

    return {'recommendations': recommendation_output, 'metrics': prf_all_day}


def calculate_prf_per_day(user, campaign, date):

    # date = data.get_first_date_of_data_test(user,campaign)
    day_duration = data.get_recommend_available_duration()

    recommend_dict = cs_compare_model_with_history(user, campaign)
    consumers = get_all_consumers_of_campaign(campaign)

    prf_all_consumer_per_day = []
    for c in consumers:

        recommend_items = recommend_dict[c.con_id]['items']
        buy_items = data.get_bought_product_id_available_times_by_consumer(user, campaign, date, day_duration, c.con_id)
        recommend_success_items = []

        for ri in recommend_items:
            for bi in buy_items:
                if ri == bi:
                    recommend_success_items.append(ri)
        recommend_success_items = list(set(recommend_success_items))

        # calculate
        if len(recommend_items) > 0 and len(buy_items) > 0:
            precision = len(recommend_success_items)/len(recommend_items)
            recall = len(recommend_success_items)/len(buy_items)
            f1 = 0
            if precision != 0 and recall != 0:
                f1 = 2/((1/precision) + (1/recall))

            prf = [precision, recall, f1]
            prf_all_consumer_per_day.append(prf)

    prf_sum = [sum(i) for i in zip(*prf_all_consumer_per_day)]
    if len(prf_sum) != 0:
        precision_average = prf_sum[0] / len(prf_all_consumer_per_day)
        recall_average = prf_sum[1] / len(prf_all_consumer_per_day)
        f1_average = prf_sum[2] / len(prf_all_consumer_per_day)

        prf_average = [precision_average, recall_average, f1_average]

        return prf_average


def calculate_prf_all(user, campaign):
    date = data.get_first_date_of_data_test(user, campaign)
    data_test = data.cut_data(user, campaign, DATA_CUTTING_MARK, type='TEST')
    end_date = data_test[-1].purchase_time
    # not count 1 day before end_date
    loop_days = (end_date - date - datetime.timedelta(days=1)).days

    prf_all_day = []
    for i in range(loop_days):
        date = date + datetime.timedelta(days= 1)
        prf = calculate_prf_per_day(user, campaign, date)
        if prf is not None:
            prf_all_day.append(prf)

    if len(prf_all_day) > 0:
        prf_sum = [sum(i) for i in zip(*prf_all_day)]
        if len(prf_sum) != 0:
            precision_average = prf_sum[0] / len(prf_all_day)
            recall_average = prf_sum[1] / len(prf_all_day)
            f1_average = prf_sum[2] / len(prf_all_day)

            prf_average = [precision_average, recall_average, f1_average]

            return prf_average
    else:
        return None


