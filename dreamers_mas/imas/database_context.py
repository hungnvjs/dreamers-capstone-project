from itertools import count

from campaign.models import *
from neomodel import db, config
import pandas as pd
import datetime

config.DATABASE_URL = 'bolt://neo4j:i-096f10099be71cf0e@13.251.238.137:7687'
# config.DATABASE_URL = 'bolt://neo4j:123456@localhost:7687'
DATA_CUTTING_MARK = 70
RECOMMEND_AVAILABLE_DURATION = 10


def get_all_clothes_id_of_campaign(campaign):
    '''

    :param campaign: object campaign
    :return: list product id with category clothes of selected campaign
    '''
    list_product_id = []
    products = get_all_clothes_of_campaign(campaign)
    for p in products:
        list_product_id.append(p.clo_id)
    return list_product_id


def get_all_consumer_id_of_campaign(campaign):
    '''

    :param campaign: object campaign
    :return: list consumer of selected campaign
    '''

    list_consumer_id = []
    consumers = get_all_consumers_of_campaign(campaign)
    for c in consumers:
        list_consumer_id.append(c.con_id)
    return list_consumer_id


def get_all_clothes_id_of_consumer(user, campaign, con_id):
    purchases = get_all_purchases_of_campaign(user, campaign)

    clothes = []

    for p in purchases:
        if con_id == p.con_id:
            clothes.append(p.clo_id)

    return clothes


def get_purchase_histories_table(user, campaign):
    purchases = cut_data(user, campaign, DATA_CUTTING_MARK)
    consumer_purchase_history = []

    for item in purchases:
        con_id = item.con_id
        clo_id = item.clo_id
        purchase_time = item.purchase_time
        quantity = item.quantity
        price = item.price

        list_column = [con_id, clo_id, purchase_time, quantity, price]
        consumer_purchase_history.append(list_column)

    purchase_histories = pd.DataFrame(data=consumer_purchase_history)

    return purchase_histories


def get_purchase_histories_table_recent_days(user, campaign, date, day_duration):
    purchases = get_purchase_histories_recent_days(user, campaign, date, day_duration)

    consumer_purchase_history = []

    for item in purchases:
        con_id = item.con_id
        clo_id = item.clo_id
        purchase_time = item.purchase_time
        quantity = item.quantity
        price = item.price

        list_column = [con_id, clo_id, purchase_time, quantity, price]
        consumer_purchase_history.append(list_column)

    purchase_histories = pd.DataFrame(data=consumer_purchase_history)

    return purchase_histories


def sort_purchase_histories_by_date_asc(purchases):
    purchases.sort(key=lambda p: p.purchase_time)
    return purchases


def cut_data(user, campaign, percent, type='MODEL'):
    data = get_all_purchases_of_campaign(user, campaign)
    sorted_data = sort_purchase_histories_by_date_asc(data)
    data_result = []
    cut_mark = len(sorted_data) * percent / 100

    for i in range(len(sorted_data)):
        if i <= cut_mark:
            data_result.append(sorted_data[i])
        else:
            break

    if type == 'MODEL':
        return data_result
    elif type == 'TEST':
        return sort_purchase_histories_by_date_asc(list(set(sorted_data) - set(data_result)))


def get_purchase_histories_recent_days(user, campaign, date, day_duration):
    end_date = date
    start_date = end_date - datetime.timedelta(days=day_duration)

    purchases = get_all_purchases_of_campaign(user, campaign)

    purchases_result = []

    for p in purchases:
        if p.purchase_time >= start_date and p.purchase_time <= end_date:
            purchases_result.append(p)

    return purchases_result


def get_bought_product_id_available_times_by_consumer(user, campaign, date, day_duration, con_id):
    start_date = date
    end_date = start_date + datetime.timedelta(days=day_duration)

    products_result = []
    purchases = get_all_purchases_of_campaign(user, campaign)
    for p in purchases:
        if p.purchase_time >= start_date and p.purchase_time <= end_date:
            if con_id == p.con_id:
                products_result.append(p.clo_id)

    return products_result


def split_day(time):
    time_split = str(time).split(' ')
    day = time_split[0]
    return day + ' '


def get_recommend_hour_of_consumer(user, campaign, con_id):
    consumers = get_all_consumers_of_campaign(campaign)

    min_average = 0
    for c in consumers:
        if con_id == c.con_id:
            purchases = get_all_purchases_of_campaign(user, campaign)
            time = 0
            count = 0
            for p in purchases:
                if p.con_id == c.con_id:
                    date = p.purchase_time
                    hour = date.hour
                    min = date.minute

                    time += hour * 60 + min
                    count += 1

            if count != 0:
                min_average = time / count
                break

    hour = int(min_average // 60)
    minute = int(min_average - hour * 60)

    if (hour - 3) <= 6:
        hour = 6
    elif (hour - 3) >= 10:
        hour = 10

    str_hour = str(hour)
    str_minute = str(minute)

    if hour < 10:
        str_hour = '0' + str(hour)
    if minute < 10:
        str_minute = '0' + str(minute)
    time_average = str_hour + ':' + str_minute

    return time_average


def get_first_date_of_data_test(user, campaign):
    data_test = cut_data(user, campaign, DATA_CUTTING_MARK, type='TEST')
    date = data_test[0].purchase_time
    return date


def set_data_cutting_mark(percent):
    global DATA_CUTTING_MARK
    DATA_CUTTING_MARK = percent


def get_recommend_available_duration(day=RECOMMEND_AVAILABLE_DURATION):
    return day
