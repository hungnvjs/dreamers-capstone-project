from campaign.models import *
# for run project
from . import cosine_similarity as cs

# for run file in command line
# import cosine_similarity as cs


def do_imas_algorithms(user, campaign):
    output = cs.cs_compare_model_with_history(user, campaign)
    return output


