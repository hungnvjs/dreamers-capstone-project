import re

from django.contrib import messages

from dreamers_mas.decorators import *
import json
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMessage
from django.db.migrations import serializer
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import render, render_to_response
import base64, logging, re

# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from .models import *

# Code for load all data in homepage
from homepage.tokens import account_activation_token


def error404(request):
    return render(request, 'error_pages/page-error-404.html')


def error500(request):
    return render(request, 'error_pages/page-error-500.html')


def index(request):
    story = OurStory.nodes.all()
    our_service = OurService.nodes.all()
    imas = IMAS.nodes.all()
    contact = OurContact.nodes.all()
    all_packages = Package.nodes.order_by('current_price')
    data = {
        'story': story[0].description,
        'our_service': our_service,
        'imas': imas[0],
        'contact': contact[0],
        'all_packages': all_packages,
    }
    return render(request, 'index.html', data)


# # Code for validate password1 password2
# def check_pass(pass1, pass2):
#     if pass1 == pass2 and pass1:  # and pass1 de tranh viec nguoi dung nhap space
#         return pass2
#     raise Exception("Password and the confirm password doesn't match")
#
#
# # Code for validate username
# def check_username(username):
#     if not re.search(r'^\w+$', username):
#         raise Exception("username must be only character")
#     try:
#         User.nodes.get(username=username)
#     except ObjectDoesNotExist:
#         return username
#     raise Exception("Please try the other username!!")
#
#
# # Code for validate Email
# def check_email(email):
#     try:
#         Information.nodes.get(email=email)
#     except ObjectDoesNotExist
#         return email
#     raise Exception("This is Email already registered.")


# Save user to Neo4j
# def save():
#     User(username, password).save()


# auto create with max number of id code in database
def generate_uid():
    cur_uid = get_max_uid()
    if not cur_uid or cur_uid is None:  # init first UID
        new_uid = "USR0"
    else:
        # max_code = int(str(cur_uid)[3:])
        new_uid = "USR" + str(cur_uid + 1)  # Create
    return new_uid


class validation():  # Class to validate Input

    def is_valid_username(username):  # validating username

        if re.match("^[a-zA-Z0-9_.-]+$", str(username)) is not None:  # Username contains only character and number
            return True
            # return False
        else:
            return False
            # return True

    def is_valid_password(pass1):
        if re.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}",
                    # Password must contains at least 8 characters and UpperCase,LowerCase,Number,unique syntax
                    str(pass1)) is not None:
            return True
            # return False
        else:

            return False
            # return True

    def is_valid_email(email):  # Validating Email

        if re.match("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", email) is not None and exist_email(
            str(email)) is True:
            return True
            # return False
        else:
            return False
            # return True

    def is_valid_phone(phone):  # Validating phone no
        if re.match("^(([0-9]|\\+)(\\d{9})|(\\d{11}))$", phone) is not None:
            return True
        else:
            return False

    def not_same_password(pass1, pass2):
        if pass1 == pass2:
            return True
        else:
            return False


# Code for register
def register(request):
    return_data = {}
    r_username = request.POST.get('username')
    r_email = request.POST.get('email')
    r_password1 = request.POST.get('password')
    r_password2 = request.POST.get('password2')
    r_uid = generate_uid()

    data = {
        'username': r_username,
        'password1': r_password1,
        'password2': r_password2,
        'email': r_email,

    }
    if validation.is_valid_username(r_username) and validation.is_valid_password(r_password1) and \
            validation.is_valid_email(r_email) and exist_username(r_username) and exist_email(r_email) is True:

        new_user = User(username=r_username, password=r_password1, uid=r_uid).save()
        # new_user.save()
        new_information = Information(email=r_email).save()
        # new_information.save()
        new_user.info.connect(new_information)

        print(new_user)
        print(new_information)
        current_site = get_current_site(request)
        message = render_to_string('acc_active_email.html', {
            'user': new_user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(new_user.uid)).decode,
            'token': account_activation_token.make_token(new_user),
        })
        # Sending activation link in terminal
        # user.email_user(subject, message)
        mail_subject = '<No-Reply> Verification E-mail'
        to_email = r_email
        email = EmailMessage(mail_subject, message, to=[to_email])
        email.send()
        return_data["error_message"] = "Success"
        return render(request, 'after_register_page.html')
    # elif exist_username(r_username) is False:
    #     return HttpResponse("Username have been registered. Plz re-enter")
    elif exist_email(r_email) is False:
        return_data["error_message"] = "This Email have been Registered"
        JsonResponse(return_data)
        # return HttpResponse("This Email have been Registered. Try Agian!")
    elif validation.is_valid_username(r_username) is False:
        return_data["error_message"] = "Invalid username"
        JsonResponse(return_data)
        # return HttpResponse('Wrong userName')
    elif validation.is_valid_password(r_password1) is False:
        return_data["error_message"] = "Invalid Password"
        JsonResponse(return_data)
        # return HttpResponse('Wrong password')
    elif validation.is_valid_email(r_email) is False:
        return_data["error_message"] = "Invalid Email"
        JsonResponse(return_data)
        # return HttpResponse('Email is Invalid')

    return JsonResponse(return_data)


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.nodes.get(uid=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.user_type = 1
        user.save_without_encrypt()
        # login(request, user)
        messages.success(request, "Thank you for your email confirmation. Now you can login your account.")
        return HttpResponseRedirect(reverse('homepage:index'))
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')


def login(request):

    if request.method == 'POST':
        try:
            username = request.POST.get('username').lower()
            password = request.POST.get('password')
            data = {}
            if username != '' and password != '':
                user = get_user_by_username(username)
                if user is not None:
                    # user = authenticate(username=username, password=password)
                    if pbkdf2_sha256.verify(password, user.password):
                        if user.user_type == 1 or user.user_type == 0:
                            if user.is_active:
                                request.session.set_expiry(7200)  # sets the exp. value of the session
                                request.session['uid'] = user.uid
                                data['error_message'] = 'Success'
                            else:
                                print(user.is_active)
                        else:
                            print(user.user_type)
                        # return HttpResponseRedirect(reverse('homepage:index'), {"uid": user.uid})
                    else:
                        print(pbkdf2_sha256.verify(password, user.password))
                        data['error_message'] = 'Username or Password is not correct. Please try again!'
                        # return JsonResponse(data)
                elif user is None:
                    data['error_message'] = 'Username or Password is not correct. Please try again!'
                    # return JsonResponse(data)
            else:
                data['error_message'] = 'Username and password must be filled'
                # return JsonResponse(data)
            return JsonResponse(data)
        except Exception as e:
            logging.getLogger("error_logger").error("login. " + repr(e))
            return HttpResponseRedirect(reverse('homepage:index'))

    else:
        return HttpResponseRedirect(reverse('homepage:index'))


def check_login(request):#check username is registered or not

    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        response_data = {}
        signup = request.POST["signup"]
        user = None
        try:
            try:
                user = get_user_by_username(username=signup)
            except ObjectDoesNotExist as e:
                pass
            except Exception as e:
                raise e
            if user is None:
                response_data["is_success"] = True
            else:
                response_data["is_success"] = False # ajax get value a show error
        except Exception as e:
            response_data["is_success"] = False
            response_data["msg"] = "Some error occurred. Please let Admin know."

        return JsonResponse(response_data)


def check_email(request): #check email is registered or not
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        response_data = {}
        signup_email = request.POST["signup_email"]
        mail_check = exist_email(email=signup_email)

        if mail_check is True:
            if validation.is_valid_email(signup_email) is True:
                response_data["is_success"] = "success"
                JsonResponse(response_data)
            else:
                response_data["is_success"] = "Email is invalid"
                JsonResponse(response_data)
        else:
            response_data["is_success"] = "This email has been registered"
            JsonResponse(response_data)

        return JsonResponse(response_data)


def check_password(request):#check input password
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        data = {}
        signup_password = request.POST["signup_password"]

        if validation.is_valid_password(signup_password) is True:
            data["error_message"] = "success"

        else:
            data[
                "error_message"] = "Password must contains:<br/> <i>At least 8 characters and UpperCase, LowerCase, Number, unique syntax</i>"

        return JsonResponse(data)


# def formView(request):#formView if has session
#
#    if request.session.has_key('username'):
#       username = request.session['username']
#       return HttpResponseRedirect(reverse('mas:index'), {"username": username})
#       # return render(request, 'index.html', {"username" : username})
#    else:
#       return HttpResponse("You're logged out.")

@login_required
def logout(request):
    try:
        del request.session['uid']
        # return HttpResponseRedirect(reverse('homepage:index'))
    except KeyError:
        pass
    # return HttpResponse("You're logged out.")
    return HttpResponseRedirect(reverse('homepage:index'))


# @login_required()

def cpanel(request):
    if request.session.has_key('uid'):
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        if user is not None:
            if user.user_type == 1:
                return HttpResponseRedirect(reverse('mas:index'))
            elif user.user_type == 0:
                return HttpResponseRedirect(reverse('admin:index'))
    else:
        return HttpResponseRedirect(reverse('homepage:index'))



def change_password(request):  # Send email to reset pasword
    return_data = {}
    input_email = request.POST.get("email").strip()
    user = get_user_by_email(email=input_email)

    if user is None:
        return_data["message"] = "This E-mail is invalid. Please try again!"
    else:
        current_site = get_current_site(request)
        message = render_to_string('reset_password.html', {
            'user': user, 'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.uid)).decode,
            'token': account_activation_token.make_token(user),
        })
        mail_subject = '<No-Reply> Reset Password E-mail'
        to_email = input_email
        email = EmailMessage(mail_subject, message, to=[to_email])
        email.send()
        return_data["error_message"] = "Success"
        return render(request, 'after_request_change_password.html')


def change_password_success(request, uidb64,
                            token):  # If user click to link on e-mail- Call this function to  reDirect changPass
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.nodes.get(uid=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        data = {
            'uid': uidb64
        }
        # login(request, user)
        return render(request, 'chane_password.html', data)
    else:
        return HttpResponse('Activation link is invalid!')


def confirm_change_pass(request):
    data = {}
    uid64 = request.POST.get('token')
    uid = force_text(urlsafe_base64_decode(uid64))
    password = request.POST.get('password')
    if uid != '' and password != '':
        user = User.nodes.get(uid=uid)
        user.password = password
        user.save()
        print(user)
        # data['mess'] = messages.success(request, "Change Password Success!!")
        # return JsonResponse(data)
        return HttpResponseRedirect(reverse('homepage:index'))
    else:
        return HttpResponse("Failed")

# def check_password_change(request):#Check password in change_password function
#     if request.method == "GET":
#         raise Http404("URL doesn't exists")
#     else:
#         data = {}
#         signup_password = request.POST["signup_password"]
#         signup_password2 = request.POST["signup_password2"]
#
#         if validation.is_valid_password(signup_password) is True and signup_password == signup_password2:
#             data["error_message"] = "success"
#
#         else:
#             data["error_message"] = "Password must contains:<br/> <i>At least 8 characters and UpperCase, LowerCase, Number, unique syntax</i>"
#
#         return JsonResponse(data)

# def validate_username(request):
#     username = request.GET.get('username', None)
#     data = {
#         'is_taken': User.NodeSet.filter(username__iexact=username).exists()
#     }
#     return JsonResponse(data)
