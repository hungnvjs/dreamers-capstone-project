from django.db import models
from dreamers_mas.models import *


# Create your models here.

def exist_email(email):  # check Exist Email
    query = "Match(n:Information) return n.email"
    results, meta = db.cypher_query(query)
    for n in results:
        if email == n[0]:
            return False
    return True


def exist_username(username):  # Check Exist Email
    query = "Match(n:User) return n.username"
    results, meta = db.cypher_query(query)
    list_user = results
    for n in list_user:
        if username == n[0]:
            return False
    return True


def get_max_uid():  # Get UserID and return MAX_User_ID
    try:
        query = "match(n:User) return n.uid"
        results, meta = db.cypher_query(query)
        uid_number_list = []
        for n in results:
            x = (int(n[0][3:]))
            # print(x)
            uid_number_list.append(x)
        uid_number_list.sort()
        max_uid = uid_number_list[len(uid_number_list) - 1]
        return max_uid
    except Exception as e:
        logging.getLogger("error_logger").error("get_max_uid " + repr(e))
        return None


def get_user_by_username(username):
    try:
        query = "match(n:User{username:'" + username + "'}) return n"
        results, meta = db.cypher_query(query)
        return [User.inflate(row[0]) for row in results][0]
    except Exception as e:
        logging.getLogger("error_logger").error("get_user_by_username " + repr(e))
        return None


def get_user_by_uid(uid):
    try:
        user = User.nodes.get_or_none(uid=uid)
        return user
    except Exception as e:
        logging.getLogger("error_logger").error("get_user_by_uid " + repr(e))
        return None


def get_user_by_email(email):
    try:
        query = "match(n:User)--(m:Information{email:'" + email + "'}) return n"
        # query = "match(n:User{username:'" + username + "'}) return n"
        results, meta = db.cypher_query(query)
        return [User.inflate(row[0]) for row in results][0]
    except Exception as e:
        logging.getLogger("error_logger").error("get_user_by_username " + repr(e))
        return None


def get_all_user():  # get all use has type>0
    query = "match(n:User) where n.user_type >0 return n"
    results, meta = db.cypher_query(query)
    list_user = results[0]
    return list_user
