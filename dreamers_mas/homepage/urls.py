from django.conf.urls import url
from django.urls import path, include
from . import views

app_name = 'homepage'
urlpatterns = [
    path('', views.index, name='index'),
    path('payment/', include('payment.urls')),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('cpanel/',views.cpanel, name='cpanel'),
    path('login/check-login', views.check_login, name='check_login'),
    path('register/check-email', views.check_email, name='check_email'),
    path('register/check-password', views.check_password, name='check_password'),
    url(r'^reset-password/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.change_password_success, name='change_password'),
    path('login/change-password', views.change_password, name='pass_change'),
    path('login/confirm-password', views.confirm_change_pass, name='confirm_change_pass'),
    # path('login/check-pass-change', views.check_password_change, name='check_pass_change'),


    # url(r'^ajax/validate_username/$', views.validate_username, name='validate_username'),
    # path('connection/',views.formView, name='loginForm')
]
