import logging
from datetime import datetime

from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse

from dreamers_mas.decorators import admin_login_required
from homepage.models import get_user_by_uid, get_user_by_username
from support.models import *
from support.views import send_subject, load_inbox, reply_message, load_read


@admin_login_required
def inbox(request):
    try:
        context = load_inbox(request)
        if request.is_ajax():
            return JsonResponse(context)
            # json_data = json.dumps(context)
            # return HttpResponse(json_data, content_type='application/json')
        else:
            return render(request, 'admin-inbox.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("admin inbox. " + repr(e))
        messages.error(request, "Can't list subject!")
        return render(request, 'admin-inbox.html')

@admin_login_required
def read(request, sub_id):
    try:
        context = load_read(request, sub_id)
        if request.is_ajax():
            return JsonResponse(context)
        else:
            return render(request, 'admin-read.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("admin-read. " + repr(e))
        messages.error(request, "Can't read!")
        return render(request, 'admin-read.html')


def compose(request):
    return render(request, 'admin-compose.html')


@admin_login_required
def send(request):
    if request.method == "POST":
        try:
            send_subject(request)

            return HttpResponseRedirect(reverse("admin:inbox"))

        except Exception as e:
            logging.getLogger("error_logger").error("send. " + repr(e))
            return HttpResponseRedirect(reverse("admin:compose"))

    else:
        return HttpResponseRedirect(reverse("admin:compose"))


@admin_login_required
def reply(request):
    if request.method == "POST":
        try:
            reply_message(request)

        except Exception as e:
            logging.getLogger("error_logger").error("reply. " + repr(e))

        return JsonResponse({})
    return JsonResponse({})


