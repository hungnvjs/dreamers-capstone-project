import logging
from django.contrib import messages
from django.http import JsonResponse, HttpResponseRedirect
from django.http import Http404, JsonResponse
from django.shortcuts import render
from django.urls import reverse

from admin_dashboard.models import *
from dashboard.models import get_info_of_user, Package
from dashboard.views import check_noti
from dreamers_mas.decorators import admin_login_required
from homepage.models import get_user_by_uid, get_all_user
from dreamers_mas.models import User
from payment.models import get_all_payments


@admin_login_required
def admin_dashboard(request):
    try:
        num_user = get_number_of_user()
        num_pending_user = get_number_of_pending_user()
        num_campaign = get_number_of_campaign()
        total_revenue = get_total_revenue()

        package_data = {
            "trial": count_all_trial_package(),
            "standard": count_all_standard_package(),
            "premium": count_all_premium_package(),
        }

        context = {
            'check_noti': check_noti(request),
            'num_user': num_user,
            'num_pending_user': num_pending_user,
            'num_campaign': num_campaign,
            'total_revenue': total_revenue,
            'package_data': package_data,
        }
        return render(request, 'admin-dashboard.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("admin_dashboard. " + repr(e))
        messages.error(request, "Can't show dashboard!")
    return render(request, 'admin-dashboard.html')


@admin_login_required
def profile(request):
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        info = get_info_of_user(user)
        if info.first_name is None:
            info.first_name = ""
        if info.last_name is None:
            info.last_name = ""
        if info.company_name is None:
            info.company_name = ""
        if info.phone is None:
            info.phone = ""
        context = {
            'user': user,
            'info': info,
        }
        return render(request, 'admin-profile.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("profile. " + repr(e))
        messages.error(request, "Can't show your profile!")
        return render(request, 'admin-dashboard.html')

#
# @admin_login_required
# def admin_manage_user(request):
#     try:
#         user_infor = []
#         users = User.nodes.filter(user_type__gt=0)
#         for i in users:
#             info = get_info_of_user(i)
#             user_data = {
#                 'username': i,
#                 'infor': info,
#
#             }
#             user_infor.append(user_data)
#         context = {
#             'info': user_infor,
#         }
#         return render(request, 'admin-manage-user.html', context)
#     except Exception as e:
#         logging.getLogger("error_logger").error("manage user. " + repr(e))
#         messages.error(request, "Can't show your profile!")
#     return render(request, 'admin-dashboard.html')
#
#
# @admin_login_required
# def change_user_status_to_deactivate(request):
#     if request.method == "GET":
#         raise Http404("URL doesn't exists")
#     else:
#         data = {}
#         user_name = request.POST["user_name"]
#         is_active = request.POST["is_active"]
#
#         user = User.nodes.get(username=user_name)
#         if user is not None and is_active == 'false':
#             user.is_active = False
#             user.save()
#             data["message"] = "success"
#     return JsonResponse(data)
#
# @admin_login_required
# def change_user_status_to_active(request):
#     if request.method == "GET":
#         raise Http404("URL doesn't exists")
#     else:
#         data = {}
#         user_name = request.POST["user_name"]
#         is_active = request.POST["is_active"]
#
#         user = User.nodes.get(username=user_name)
#         if user is not None and is_active == 'true':
#             user.is_active = True
#             user.save()
#             data["message"] = "success"
#     return JsonResponse(data)


@admin_login_required
def load_unread_mail(request):
    try:
        if request.is_ajax():
            context = check_noti(request)
            return JsonResponse(context)
        else:
            return HttpResponseRedirect(reverse("admin:dashboard"))
    except Exception as e:
        logging.getLogger("error_logger").error("load_unread_mail. " + repr(e))



@admin_login_required
def all_payment_history(request):
    try:
        all_payments = get_all_payments()
        pay_table = []
        for pay in all_payments:
            pay_table.append({
                'payment': pay,
                'package': Package.nodes.filter(pid=pay.pid)[0],
            })

        context = {
            'pay_table': pay_table,
        }
        return render(request, 'admin-payment-history.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("all_payment_history. " + repr(e))
        messages.error(request, "Can't list payment!")
        return render(request, 'admin-payment-history.html')


# @admin_login_required
# def admin_dashboard(request):
#
#     num_user = get_number_of_user()
#     print(num_user)