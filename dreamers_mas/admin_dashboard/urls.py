from django.urls import path, include
from . import views

app_name = 'admin'
urlpatterns = [
    path('', views.admin_dashboard, name='index'),
    path('dashboard/', views.admin_dashboard, name='dashboard'),
    path('profile/', views.profile, name='profile'),
    path('all-payment-history/', views.all_payment_history, name='all_payment_history'),

    # path('manage-user/', views.admin_manage_user, name='manage_user'),
    # path('manage-user/change-status-to-deactivate/', views.change_user_status_to_deactivate, name='change_user_status_to_deactivate'),
    # path('manage-user/change-status-to-active/', views.change_user_status_to_active, name='change_user_status_to_active'),

    path('noti/', views.load_unread_mail, name='load_unread_mail'),

    path('support/', include('admin_support.urls')),
    path('manage-package/', include('manage_package.urls')),
    path('manage-user/', include('manage_user.urls')),

]
