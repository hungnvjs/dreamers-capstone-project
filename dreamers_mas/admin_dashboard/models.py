from django.db import models

# Create your models here.
from dreamers_mas.models import *


def get_number_of_user():  # count number of user
    query = "match(u:User{is_active:true}) where u.user_type>0 return count(u)"
    results, meta = db.cypher_query(query)
    list_user = results[0]
    return list_user[0]


def get_number_of_pending_user():
    query = "match (n:User) where n.user_type= 2 return count(n)"
    results, meta = db.cypher_query(query)
    list_user = results[0]
    return list_user[0]


def get_number_of_campaign():
    query = "match (n:Campaign) return count(n)"
    results, meta = db.cypher_query(query)
    list_user = results[0]
    return list_user[0]


def get_total_revenue():
    revenue = 0
    query = "match(u:User)-[r:USES]->(p:Package) return r.price"
    results, meta = db.cypher_query(query)

    for total in results:
        revenue += total[0]

    return revenue


def count_all_trial_package():
    try:
        query = "match(u:User)-[r:USES]->(p:Package) where r.pid = 'PKG1' return count(r)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("count_all_trial_package: " + repr(e))
        return 0


def count_all_standard_package():
    try:
        query = "match(u:User)-[r:USES]->(p:Package) where r.pid = 'PKG2' return count(r)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("count_all_standard_package: " + repr(e))
        return 0


def count_all_premium_package():
    try:
        query = "match(u:User)-[r:USES]->(p:Package) where r.pid = 'PKG3' return count(r)"
        results, meta = db.cypher_query(query)
        count = results[0][0]
        if count is None:
            return 0
        return count
    except Exception as e:
        logging.getLogger("error_logger").error("count_all_premium_package: " + repr(e))
        return 0
