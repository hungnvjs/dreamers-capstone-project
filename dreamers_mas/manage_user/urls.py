from django.urls import path
from . import views


urlpatterns = [
    # path('', views.manage_user, name='manage_user'),
    # path('profile/', views.profile, name='profile'),
    path('', views.admin_manage_user, name='manage_user'),
    path('change-status-to-deactivate/', views.change_user_status_to_deactivate,
         name='change_user_status_to_deactivate'),
    path('change-status-to-active/', views.change_user_status_to_active,
         name='change_user_status_to_active'),
]