import logging

from django.contrib import messages
from django.http import Http404, JsonResponse
from django.shortcuts import render

# Create your views here.
#
# def manage_user(request):
#     return render(request, 'manage-user.html')
from dashboard.models import get_info_of_user
from dreamers_mas.decorators import admin_login_required
from homepage.models import get_user_by_uid, User


@admin_login_required
def admin_manage_user(request):
    try:
        user_infor = []
        users = User.nodes.filter(user_type__gt=0)
        for i in users:
            info = get_info_of_user(i)
            user_data = {
                'username': i,
                'infor': info,

            }
            user_infor.append(user_data)
        context = {
            'info': user_infor,
        }
        return render(request, 'manage-user.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("manage user. " + repr(e))
        messages.error(request, "Can't show your profile!")
    return render(request, 'admin-dashboard.html')


@admin_login_required
def change_user_status_to_deactivate(request):
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        data = {}
        user_name = request.POST["user_name"]
        is_active = request.POST["is_active"]

        user = User.nodes.get(username=user_name)
        if user is not None and is_active == 'false':
            user.is_active = False
            user.save_without_encrypt()
            data["message"] = "success"
    return JsonResponse(data)


@admin_login_required
def change_user_status_to_active(request):
    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        data = {}
        user_name = request.POST["user_name"]
        is_active = request.POST["is_active"]

        user = User.nodes.get(username=user_name)
        if user is not None and is_active == 'true':
            user.is_active = True
            user.save_without_encrypt()
            data["message"] = "success"
    return JsonResponse(data)
