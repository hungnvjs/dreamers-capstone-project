from django.apps import AppConfig


class ManagePackageConfig(AppConfig):
    name = 'manage_package'
