from django.urls import path
from . import views


urlpatterns = [
    path('', views.manage_package, name='manage_package'),
    path('edit/', views.edit_package, name='edit_package'),
]