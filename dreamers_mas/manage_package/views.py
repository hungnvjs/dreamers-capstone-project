import logging

from django.contrib import messages
from django.http import Http404, JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from dreamers_mas.decorators import admin_login_required
from dreamers_mas.models import Package


@admin_login_required
def manage_package(request):

    try:
        all_packages = Package.nodes.order_by('current_price')
        context = {
            'all_packages': all_packages,
        }
        return render(request, 'manage-package.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("payment. " + repr(e))
        messages.error(request, "Can't list payment!")
        return render(request, 'dashboard.html')
def edit_package(request):

    if request.method == "GET":
        raise Http404("URL doesn't exists")
    else:
        data = {}
        try:

            pkg_name = request.POST["pgk_name"]
            pkg_max_campaign = int(request.POST["pkg_campaign"])
            pkg_max_consumer = int(request.POST["pkg_consumer"])
            pkg_max_product = int(request.POST["pkg_product"])
            pkg_max_purchase_history = int(request.POST["pkg_purchase_history"])
            pkg_current_price = float(request.POST["pkg_price"])
            pkg_max_duration = int(request.POST["pkg_duration"])
        except Exception as e:
            messages.error(request, "All fields must be digits!! ")
            return render(request,"manage-package.html")

        package = Package.nodes.get(pname=pkg_name)
        if package is not None :
            package.max_campaign = pkg_max_campaign
            package.max_consumer = pkg_max_consumer
            package.max_clothes = pkg_max_product
            package.max_purchase_history = pkg_max_purchase_history
            package.current_price = pkg_current_price
            package.duration = pkg_max_duration
            package.save()
            data["message"] = "success"
    return JsonResponse(data)