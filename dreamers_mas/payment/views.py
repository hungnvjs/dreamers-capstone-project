import logging
from datetime import datetime, timedelta

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from campaign.models import get_current_package_of_user, get_all_campaigns_of_user
from dashboard.views import check_expire
from dreamers_mas.decorators import user_login_required
from homepage.models import get_user_by_uid
from .models import *


@user_login_required
def payment(request):
    check_expire(request)
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        cur_package = get_current_package_of_user(user)
        all_packages = Package.nodes.order_by('current_price')
        # if you bought package, not show trial package in web to buy more
        if cur_package is not None:
            all_packages = Package.nodes.exclude(pid="PKG1").order_by('current_price')

        context = {
            'all_packages': all_packages,
            'cur_package': cur_package,
        }
        return render(request, 'payment.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("payment. " + repr(e))
        messages.error(request, "Can't list payment!")
        return render(request, 'dashboard.html')


@user_login_required
def payment_history(request):
    check_expire(request)
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        cur_package = get_current_package_of_user(user)
        all_payments = get_all_payments_by_uid(uid)
        cur_pay = get_current_payment(uid)
        pay_table = []
        for pay in all_payments:
            pay_table.append({
                'payment': pay,
                'package': Package.nodes.filter(pid=pay.pid)[0],
            })

        context = {
            'cur_package': cur_package,
            'pay_table': pay_table,
            'cur_pay': cur_pay,
            'user': user,
        }
        return render(request, 'payment-history.html', context)
    except Exception as e:
        logging.getLogger("error_logger").error("payment. " + repr(e))
        messages.error(request, "Can't list payment!")
        return render(request, 'payment-history.html')


@user_login_required
def pay_package(request, pid):
    check_expire(request)
    try:
        uid = request.session['uid']
        user = get_user_by_uid(uid)
        cur_pay = get_current_payment(uid)
        cur_package = get_current_package_of_user(user)

        if cur_pay is not None:
            # disable current payment
            cur_pay.status = 1
            cur_pay.save()

        # create new payment
        new_package = Package.nodes.get_or_none(pid=pid)
        start_date = datetime.now()
        new_pay = user.package.connect(new_package, {
            'uid': uid,
            'pid': pid,
            'price': new_package.current_price,
            'start_date': start_date,
            'end_date': start_date + timedelta(days=new_package.duration),
            'status': 0,
        })
        new_pay.save()

        # update all campaign's package of user
        all_cams = get_all_campaigns_of_user(user)
        for cam in all_cams:
            cam.pid.disconnect(cur_package)
            cam.pid.connect(new_package)
            cam.end_date = new_pay.end_date
            cam.save()

        messages.success(request, "You bought the " + new_package.pname + " successfully!")
        return HttpResponseRedirect(reverse("mas:payment_history"))
    except Exception as e:
        logging.getLogger("error_logger").error("payment. " + repr(e))
        messages.error(request, "Can't purchase package!")
        return render(request, 'payment.html')


def update_campaign_package(user):
    try:
        all_cams = get_all_campaigns_of_user(user)
        for cam in all_cams:
            cam.pid.disconnect()
    except Exception as e:
        logging.getLogger("error_logger").error("update_campaign_package. " + repr(e))
        return None
