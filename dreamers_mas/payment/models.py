from dreamers_mas.models import *
from neomodel import OUTGOING, INCOMING, EITHER, Traversal, db
import logging


def get_all_payments_by_uid(uid):
    try:
        query = "match(u:User{uid:'" + uid + "'})-[r:USES]->(p:Package) return r order by r.start_date"
        results, meta = db.cypher_query(query)
        return [Payment.inflate(row[0]) for row in results]
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_payments_by_uid. " + repr(e))
        return None


def get_all_payments():
    try:
        query = "match(u:User)-[r:USES]->(p:Package) return r order by r.start_date"
        results, meta = db.cypher_query(query)
        return [Payment.inflate(row[0]) for row in results]
    except Exception as e:
        logging.getLogger("error_logger").error("get_all_payments_by_uid. " + repr(e))
        return None


def get_current_payment(uid):
    try:
        all_payment = get_all_payments_by_uid(uid)
        cur_pay = all_payment[len(all_payment) - 1]
        return cur_pay
    except Exception as e:
        logging.getLogger("error_logger").error("get_current_payment. " + repr(e))
        return None

