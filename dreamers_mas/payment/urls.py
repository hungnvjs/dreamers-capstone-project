from django.urls import path
from . import views


urlpatterns = [
    path('', views.payment, name='payment'),
    path('pay-package/<pid>', views.pay_package, name='pay_package'),
    path('payment-history/', views.payment_history, name='payment_history'),
]