from neomodel import (StructuredNode, StringProperty, IntegerProperty,
                      UniqueIdProperty, RelationshipTo, RelationshipFrom, EmailProperty, FloatProperty, BooleanProperty,
                      DateTimeProperty)
from neomodel.relationship import *
from django.db import models
from passlib.handlers.pbkdf2 import pbkdf2_sha256
import logging


class Payment(StructuredRel):
    uid = StringProperty()
    pid = StringProperty()
    price = FloatProperty()
    start_date = DateTimeProperty()
    end_date = DateTimeProperty()

    # --status
    # 0: Available
    # 1: Expire
    status = BooleanProperty()


class User(StructuredNode):
    uid = StringProperty(unique_index=True)
    username = StringProperty(unique_index=True)
    password = StringProperty()
    is_active = BooleanProperty(default=False)

    # --user_type
    # 0: Admin
    # 1: End User
    user_type = IntegerProperty(default=2)

    def save(self, *args, **kwargs):  # Code for encrypt Password
        self.password = pbkdf2_sha256.encrypt(self.password, rounds=12000, salt_size=32)
        super(User, self).save(*args, **kwargs)
        return self

    def verify_password(self, raw_password):  # Code for verify Password
        return pbkdf2_sha256.verify(raw_password, self.password)

    # relationship
    info = RelationshipTo("Information", "HAS")
    package = RelationshipTo("Package", "USES", model=Payment)
    campaign = RelationshipTo("Campaign", "OWNS")


class Information(StructuredNode):
    first_name = StringProperty()
    last_name = StringProperty()
    company_name = StringProperty()
    email = EmailProperty(unique_index=True)
    phone = StringProperty()

    # relationship
    user = RelationshipFrom(User, "HAS")


class Campaign(StructuredNode):
    cam_id = StringProperty()  # not unique_index=True because cam_id may be duplicate with other User
    cam_name = StringProperty()
    start_date = DateTimeProperty()
    end_date = DateTimeProperty()
    #--status
    # 0: Enable
    # 1: Disable
    status = IntegerProperty(default=0)

    # relationship
    uid = RelationshipFrom(User, "OWNS")
    pid = RelationshipTo("Package", "BELONGS_TO")
    channel_id = RelationshipTo("Channel", "USES")
    clothes = RelationshipTo("Clothes", "HAS")
    consumer = RelationshipTo("Consumer", "HAS")



    def get_clothes_by_id(self, clo_id):
        results, columns = self.cypher("match(n:Clothes {clo_id:'{clo_id}'})--(m:{self}) return n")
        return [self.inflate(row[0]) for row in results]



class Package(StructuredNode):
    pid = StringProperty(unique_index=True)
    pname = StringProperty(unique_index=True)
    current_price = FloatProperty()
    duration = IntegerProperty()
    max_campaign = IntegerProperty()
    max_consumer = IntegerProperty()
    max_clothes = IntegerProperty()
    max_purchase_history = IntegerProperty()

    # relationship
    owner = RelationshipFrom(User, "USES", model=Payment)
    channel = RelationshipTo("Channel", "CONTAINS")
    cam = RelationshipFrom(Campaign, "BELONGS_TO")


class Channel(StructuredNode):
    channel_id = StringProperty(unique_index=True)
    channel_name = StringProperty(unique_index=True)

    # relationship
    package = RelationshipFrom(Package, "CONTAINS")
    cam = RelationshipFrom(Campaign, "USES")


class PurchaseHistory(StructuredRel):
    con_id = StringProperty()
    clo_id = StringProperty()
    purchase_time = DateTimeProperty()
    quantity = IntegerProperty()
    price = FloatProperty()


class Clothes(StructuredNode):
    clo_id = StringProperty()
    name = StringProperty()

    #--age range 13 - 26
    age_range = StringProperty()
    season = StringProperty()
    material = StringProperty()
    color = StringProperty()
    price = FloatProperty()
    style = StringProperty()

    #--gender
    # 0: Male
    # 1: Female
    gender = IntegerProperty()
    brand = StringProperty()

    #--clo_type
    # 0: Áo
    # 1: Quần
    clo_type = IntegerProperty()

    # relationship
    campaign = RelationshipFrom(Campaign, "HAS")
    consumer = RelationshipFrom("Consumer", "BUYS", model=PurchaseHistory)


class Consumer(StructuredNode):
    con_id = StringProperty()
    name = StringProperty()
    age = IntegerProperty()

    #--gender
    # 0: Male
    # 1: Female
    gender = IntegerProperty()
    email = EmailProperty()
    phone_number = StringProperty()
    facebook_id = StringProperty()
    zalo_id = StringProperty()
    address = StringProperty()

    # relationship
    campaign = RelationshipFrom(Campaign, "HAS")
    clothes = RelationshipTo("Clothes", "BUYS", model=PurchaseHistory)


class OurStory(StructuredNode):
    description = StringProperty()


class OurService(StructuredNode):
    title = StringProperty()
    description = StringProperty()


class IMAS(StructuredNode):
    title = StringProperty()
    short_description = StringProperty()
    description = StringProperty()

class OurPricing(StructuredNode):
    short_description = StringProperty()
    description = StringProperty()

class OurTeam(StructuredNode):
    short_description = StringProperty()


class OurContact(StructuredNode):
    short_description = StringProperty()
    address = StringProperty()
    phone_number = StringProperty()
    email = StringProperty()


