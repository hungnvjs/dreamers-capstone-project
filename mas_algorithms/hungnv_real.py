import string
import random
from math import ceil
import uuid


def round_float_number(number):
    return ceil(number*100)/100


def random_string_generator(size=4, chars=string.ascii_lowercase + string.digits):
    return "".join(random.choice(chars)for x in range(size))


def random_email_basic_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return "".join(random.choice(chars)for x in range(size)) + "@gmail.com"


def random_phone_basic_generator(size=10):
    temp = [random.randint(0, 9) for x in range(0, size - 2)]
    phone = "09"
    for x in temp:
        phone += str(x)
    return phone


def random_price_in_range_generator(start, end):
    return round_float_number(random.uniform(start, end))


def random_age_in_range_generator(start, end):
    return random.randint(start, end)


def random_gender_generator():
    return random.choice([True, False])


def random_facebook_generator():

    temp = [random.randint(0, 9) for x in range(0, 9)]
    id = "100005"
    for x in temp:
        id += str(x)
    return id


def random_city_generator():
    return random.choice(["Ha Noi", "Ho Chi Minh", "Da Nang"])


def random_season_generator():
    return random.choice(["Autumn", "Spring", "Summer", "Winter"])


def random_material_generator():
    return random.choice(["Cotton", "Charvet", "Camlet", "Fabric", ])


def random_color_generator():
    return random.choice(["Black", "Blue", "Red", "Green", "Yellow", "Pink", "White"])


def random_style_generator():
    return random.choice(["Long", "Short"])


def random_brand_generator():
    return random.choice(["Adidas", "Nike", "Balanciaga", "Gucci", "Chanel", "Hermes", "Louis Vuitton", "Puma",])


def random_type_generator():
    return random.choice(["Sneaker", "Jackets‎", "Armwear", "Shirt", "Jeans"])


def random_clothes_name_generator(brand):
    return brand + " " + random.choice(["Jordan", "R1", "C", "Air", "Wind", "Boost"]) + " " + random_string_generator().upper()


