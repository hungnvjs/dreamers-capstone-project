from neomodel import (config, StructuredNode, StructuredRel, StringProperty, IntegerProperty,
                      UniqueIdProperty, RelationshipTo, RelationshipFrom, EmailProperty, FloatProperty, BooleanProperty,
                      DateTimeProperty)

from  neomodel.relationship import *


class Homapage(StructuredNode):
    #--status
    # 0: Active
    # 1: Maintain
    status = IntegerProperty(default=0)
    story = RelationshipTo("OurStory", "HAS")
    service = RelationshipTo("OurService", "HAS")
    imas = RelationshipTo("IMAS", "HAS")
    pricing = RelationshipTo("OurPricing", "HAS")
    team = RelationshipTo("OurTeam", "HAS")
    contact = RelationshipTo("OurContact", "HAS")

class OurStory(StructuredNode):
    description = StringProperty()


class OurService(StructuredNode):
    title = StringProperty()
    description = StringProperty()


class IMAS(StructuredNode):
    title = StringProperty()
    short_description = StringProperty()
    description = StringProperty()

class OurPricing(StructuredNode):
    short_description = StringProperty()
    description = StringProperty()

class OurTeam(StructuredNode):
    short_description = StringProperty()


class OurContact(StructuredNode):
    short_description = StringProperty()
    address = StringProperty()
    phone_number = StringProperty()
    email = StringProperty()
