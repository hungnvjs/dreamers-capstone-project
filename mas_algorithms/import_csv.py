from neomodel import clear_neo4j_database, db, config
from datetime import datetime
import csv
import datetime

from core_models import *

config.DATABASE_URL = 'bolt://neo4j:i-096f10099be71cf0e@13.251.238.137:7687'


@db.transaction
def delete_all():
    check = input("delete all?")
    if check == "ok":
        clear_neo4j_database(db)


@db.transaction
def create_package():
    with open('csv/Package.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            pid = row['pid']
            pname = row['pname']
            current_price = row['current_price']
            duration = row['duration']
            max_campaign = row['max_campaign']
            max_consumer = row['max_consumer']
            max_clothes = row['max_clothes']
            max_purchase_history = row['max_purchase_history']

            Package(pid=pid, pname=pname, current_price=current_price, duration=duration,
                    max_campaign=max_campaign, max_consumer=max_consumer, max_clothes=max_clothes,
                    max_purchase_history=max_purchase_history).save()


@db.transaction
def create_channel():
    with open('csv/Channel.csv', newline='', encoding='utf-8') as csvfile:

        package_trial = Package.nodes.get(pid='PKG1')
        package_standard = Package.nodes.get(pid='PKG2')
        package_premium = Package.nodes.get(pid='PKG3')

        reader = csv.DictReader(csvfile)
        for row in reader:
            channel_id = row['channel_id']
            channel_name = row['channel_name']

            channel = Channel(channel_id=channel_id, channel_name=channel_name).save()

            # rel package
            if channel_id == 'CHA1' or channel_id == 'CHA2':
                channel.package.connect(package_trial)
                channel.package.connect(package_standard)
                channel.package.connect(package_premium)
            elif channel_id == 'CHA3':
                channel.package.connect(package_standard)
                channel.package.connect(package_premium)
            elif channel_id == 'CHA4':
                channel.package.connect(package_premium)


@db.transaction
def create_user():
    package_trial = Package.nodes.get(pid='PKG1')
    reader_payment = []

    with open('csv/Payment.csv', newline='', encoding='utf-8') as payment_file:
        reader_payment.extend(csv.DictReader(payment_file))

    with open('csv/User.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        i = 0
        for row in reader:
            uid = row['uid']
            username = row['username']
            password = row['password']
            user_type = row['user_type']
            is_active = row['is_active']
            user = User(uid=uid, username=username, password=password, is_active=is_active, user_type=user_type).save()

            # rel package
            if i < 15:
                for pay in reader_payment:
                    uid = pay['uid']
                    pid = pay['pid']
                    price = pay['price']
                    start_date = pay['start_date']
                    end_date = pay['end_date']

                    if pid == package_trial.pid:
                        if (uid == uid):
                            user.package.connect(package_trial, {
                                'uid': uid,
                                'pid': pid,
                                'price': price,
                                'start_date': datetime.datetime.strptime(start_date, "%Y-%m-%d %H:%M"),
                                'end_date': datetime.datetime.strptime(end_date, "%Y-%m-%d %H:%M")

                            })
                            break
            i += 1


@db.transaction
def create_information():
    users = User.nodes.all()
    with open('csv/Information.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        i = 0
        for row in reader:
            first_name = row['first_name']
            last_name = row['last_name']
            company_name = row['company_name']
            email = row['email']
            phone = row['phone']

            info = Information(first_name=first_name, last_name=last_name, company_name=company_name,
                               email=email, phone=phone).save()

            info.user.connect(users[i])
            i += 1


@db.transaction
def create_clothes():
    cam = Campaign.nodes.get(cam_id='CAM2')
    with open('csv/Clothes.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            clo_id = row['clo_id']
            name = row['name']
            age_range = row['age']
            season = row['season']
            material = row['material']
            color = row['color']
            price = row['price']
            style = row['style']
            gender = row['gender']
            brand = row['brand']
            clo_type = row['clo_type']

            clo = Clothes(clo_id=clo_id, name=name, age_range=age_range, season=season, material=material,
                          color=color, price=price, style=style, gender=gender, brand=brand, clo_type=clo_type).save()

            # rel campaign
            clo.campaign.connect(cam)


@db.transaction
def create_consumer():
    cam = Campaign.nodes.get(cam_id='CAM2')
    history_reader = []
    with open('csv/PurchaseHistory.csv', newline='', encoding='utf-8') as history:
        history_reader.extend(csv.DictReader(history))

    with open('csv/Consumer.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            con_id = row['con_id']
            name = row['name']
            age = row['age']
            gender = row['gender']
            email = row['email']
            phone_number = row['phone_number']
            facebook_id = row['facebook_id']
            zalo_id = row['zalo_id']
            address = row['address']

            con = Consumer(con_id=con_id, name=name, age=age, gender=gender, email=email,
                           phone_number=phone_number, facebook_id=facebook_id, zalo_id=zalo_id, address=address).save()

            # rel product
            for his in history_reader:
                consumer_id = his['con_id']
                clo_id = his['clo_id']
                buy_time = his['buy_time']
                quantity = his['quantity']
                price = his['price']

                if consumer_id == con_id:
                    clothes = Clothes.nodes.get(clo_id=clo_id)
                    con.clothes.connect(clothes, {
                        'con_id': consumer_id,
                        'clo_id': clo_id,
                        'purchase_time': datetime.datetime.strptime(buy_time, "%Y-%m-%d"),
                        'quantity': quantity,
                        'price': price
                    })
                    break

            # rel campaign
            con.campaign.connect(cam)


@db.transaction
def create_package():
    with open('csv/Package.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            pid = row['pid']
            pname = row['pname']
            current_price = row['current_price']
            duration = row['duration']
            max_campaign = row['max_campaign']
            max_consumer = row['max_consumer']
            max_clothes = row['max_clothes']
            max_purchase_history = row['max_purchase_history']

            Package(pid=pid, pname=pname, current_price=current_price, duration=duration,
                    max_campaign=max_campaign, max_consumer=max_consumer, max_clothes=max_clothes,
                    max_purchase_history=max_purchase_history).save()


@db.transaction
def create_campaign():
    package_trial = Package.nodes.get(pid='PKG1')
    channel_email = Channel.nodes.get(channel_id='CHA1')
    channel_messenger = Channel.nodes.get(channel_id='CHA2')

    with open('csv/Campaign.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        count = 0
        for row in reader:
            count += 1
            user = User.nodes.get(uid='USR' + str(count))
            cam_id = row['cam_id']
            cam_name = row['cam_name']
            start_date = row['start_date']
            end_date = row['end_date']

            cam = Campaign(cam_id=cam_id, cam_name=cam_name, start_day=start_date,
                           end_day=end_date).save()
            # rel user
            cam.uid.connect(user)
            # rel package
            cam.pid.connect(package_trial)
            # rel channel
            cam.channel_id.connect(channel_email)
            cam.channel_id.connect(channel_messenger)


@db.transaction
def create_test_clothes():
    cam = Campaign.nodes.get(cam_id='1')
    clo = Clothes(clo_id='60').save()

    # rel campaign
    clo.campaign.connect(cam)


# @db.transaction
# def get_model_test():
# cam = Campaign.nodes.get(cam_id='1')
#
# clo = cam.get_clothes_by_id('60')
# query = "match(n:Clothes {clo_id:'60'})--(m:Campaign {cam_id:'1'}) return n"
# results, meta = db.cypher_query(query)
# clo = [Clothes.inflate(row[0]) for row in results]

# clo = get_clothes_by_id(60, 1)
# print(clo)
def get_last_payment_of_user(user):
    try:
        query = "MATCH(u: User {uid: '" + user.uid + "'})" \
                                                     "-[r: USES]->(p:Package) RETURN r ORDER BY r.start_date DESC LIMIT 1"
        results, meta = db.cypher_query(query)
        return [Payment.inflate(row[0]) for row in results][0]
    except Exception as e:
        # logging.getLogger("error_logger").error("get_last_payment_of_user: " + repr(e))
        return None


def update_expire():
    user = User.nodes.get(username='sangnv')
    pay = get_last_payment_of_user(user)
    pay.end_date = datetime.datetime.now()
    pay.save()


def diff_time():
    t1 = datetime.datetime.now()
    t2 = datetime.datetime.now()
    dif = t2 - t1
    dif2 = t1 - t2
    print(dif)
    print(dif2)

# ---DON'T TOUCH
# diff_time()
# update_expire()
delete_all()
create_package()
create_channel()
create_user()
create_information()
# create_campaign()
# create_clothes()
# create_consumer()

# create_test_clothes()
# get_model_test()
