from hungnv_real import *

from neomodel import clear_neo4j_database, db, config
from datetime import datetime
import random

from core_models import *

config.DATABASE_URL  = 'bolt://neo4j:i-096f10099be71cf0e@bolt://13.251.238.137:7687'


    
@db.transaction
def delete_all():
    check = input("delete all?")
    if check == "ok":
        clear_neo4j_database(db)


@db.transaction
def create_packages_channel():

    # init
    package_basic = Package(pid=1, pname="Basic", current_price=0, duration = 30, 
                            max_campaign=1, max_consumer=500, max_channel= 1).save()
    package_advanced = Package(pid=2, pname="Advanced", current_price=1000, duration = 60,
                            max_campaign=3, max_consumer=2000, max_channel = 3).save()
    channel_facebook = Channel(channel_id=1, channel_name="Facebook").save()
    channel_email = Channel(channel_id=2, channel_name="Email").save()
    channel_sms = Channel(channel_id=3, channel_name="SMS").save()

    # user - package
    for x in User.nodes.all():
        if x.username not in {"username 1", "username 3", "username 5"}:

            x.package.connect(package_basic, {"price": package_basic.current_price})
        else:
            x.package.connect(package_advanced, {"price": package_advanced.current_price})

    # package - channel
    package_basic.channel.connect(channel_facebook, {
                                      "package_id": package_basic.pid, "channel_id": channel_facebook.channel_id})
    package_advanced.channel.connect(channel_facebook, {
        "package_id": package_advanced.pid, "channel_id": channel_facebook.channel_id})

    package_advanced.channel.connect(channel_email, {
                                     "package_id": package_advanced.pid, "channel_id": channel_email.channel_id})
    package_advanced.channel.connect(channel_sms, {
                                     "package_id": package_advanced.pid, "channel_id": channel_sms.channel_id})


@db.transaction
def create_users():
    for x in range(0,10):
        user_temp = User(username="username " + str(x), password="123").save()
        info_temp = Information(first_name="First " + str(x), email= random_email_basic_generator(), 
        phone=random_phone_basic_generator()).save()
        info_temp.user.connect(user_temp)


@db.transaction
def create_campaign_fixed_user():
    cam = Campaign(cam_name="Adidas 1000 Product", start_date=str(datetime.now())).save()
    user2 = User.nodes.get(username="username 2")
    pack1 = Package.nodes.get(pid=1)
    chan1 = Channel.nodes.get(channel_id=1)
    cam.uid.connect(user2)
    cam.pid.connect(pack1)
    cam.channel_id.connect(chan1)


@db.transaction
def create_consumer():
    cam = Campaign.nodes.get(cam_name="Adidas 1000 Product")
    # number of consumer:10
    for x in range(10):
        con = Consumer(name = "Consumer " +str(x),age=random_age_in_range_generator(19,40),
        gender=random_gender_generator(),
                       email = random_email_basic_generator(),phone_number=random_phone_basic_generator(),
                       facebook_id=random_facebook_generator(), zalo_id= random_phone_basic_generator(),
                       address = random_city_generator()).save()

        con.campaign.connect(cam)

@db.transaction
def create_clothes():
    cam = Campaign.nodes.get(cam_name="Adidas 1000 Product")
    # number of clothes: 80
    for x in range(10):
        brand = random_brand_generator()
        clo = Clothes(name=random_clothes_name_generator(brand),
                      age=random_age_in_range_generator(19, 40), season=random_season_generator(), 
                      material = random_material_generator(),color = random_color_generator(),
                      price = random_price_in_range_generator(200,20000), style = random_style_generator(),
                      gender=random_gender_generator(), brand=brand, ctype=random_type_generator()).save()
        clo.campaign.connect(cam)

@db.transaction
def create_buy_history():
    consumer_list = Consumer.nodes.all()
    clothes_list = Clothes.nodes.all()
    for x in consumer_list:
        for time in range(0, random.randint(0,6)):
            qua = random.randint(1, 3)
            pos = random.randint(1, len(clothes_list))
            x.clothes.connect(clothes_list[pos-1], {
                "con_id": x.con_id, "clo_id": clothes_list[pos-1].clo_id, "buy_time":
                str(datetime.now()), "quantity": qua, "price": clothes_list[pos-1].price})




#-- không tự ý nghịch lung tung những def dưới:

#---core system
# create_users()
# create_packages_channel()
# create_campaign_fixed_user()
# create_consumer()
# create_clothes()
# create_buy_history()

#---common system



# delete_all()






