from itertools import count

from core_models import *
from neomodel import db, config
from neomodel.match import Traversal, OUTGOING, INCOMING, EITHER



config.DATABASE_URL  = 'bolt://neo4j:i-096f10099be71cf0e@bolt://13.251.238.137:7687'


@db.transaction
def get_all_consumers():
    list_consumers = Consumer.nodes.all()
    return list_consumers

@db.transaction
def get_all_products():
    list_products = Clothes.nodes.all()
    return list_products


LIST_CONSUMERS = get_all_consumers()
LIST_PRODUCTS = get_all_products()
MATRIX_ROWS = {}



@db.transaction
def get_input_matrix_dictionary():
    definition = dict(node_class=Clothes, direction=OUTGOING,
                      relation_type=None, model=BuyHistory)

    for c in LIST_CONSUMERS:
        relations_traversal = Traversal(c, Clothes.__label__, definition).all()
        consumer_buy_row = []

        # get all relations for eact consumer
        for r in relations_traversal:
            consumer_buy_row.append(r.clo_id)

        # a row of input matrix
        a_matrix_row = []

        # init all 0 for a row
        for pos in range(0,len(LIST_PRODUCTS)):
            a_matrix_row.append(0)

        # fill 1 to matched pos
        for pos in range(0,len(LIST_PRODUCTS)):
            for i in range(0,len(consumer_buy_row)):
                if LIST_PRODUCTS[pos].clo_id == consumer_buy_row[i]:
                    a_matrix_row[pos] = 1

        MATRIX_ROWS[str(c.con_id)] = a_matrix_row

    return  MATRIX_ROWS

@db.transaction
def get_product_ids_by_con_id(con_id):
    c = Consumer.nodes.get(con_id=con_id)

    definition = dict(node_class=Clothes, direction=OUTGOING,
                      relation_type=None, model=BuyHistory)
    relations_traversal = Traversal(c, Clothes.__label__, definition).all()
    products = []
    for r in relations_traversal:
        products.append(r.clo_id)

    return products

@db.transaction
def get_product_by_id(clo_id):
    product = Clothes.nodes.get(clo_id = clo_id)
    return product










