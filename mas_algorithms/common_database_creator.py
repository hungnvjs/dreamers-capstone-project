from common_models import *
from neomodel import clear_neo4j_database, db

config.DATABASE_URL  = 'bolt://neo4j:i-096f10099be71cf0e@13.251.238.137:7687'



@db.transaction
def create_story():
    DESCRIPTION = "We are always craving to provide a simple but effective product to support retailers who don’t have much experience and resources for carrying big marketing campaigns. By applying modern advance technologies, we have delivered our product for the specific target customer, who is retailer to enhance and compete with big companies and corporations in consolidating their own market share. With only 1 click, our customers have already hold in their hands one of the most advance technology to solve the target customer obstacle, the biggest problem nowadays which influence directly to their revenue."

    OurStory(description = DESCRIPTION).save()


@db.transaction
def create_service():
    TITLE_1 = "Structure"
    DESCRIPTION_1 = "Designed specifically for retailers with minimum requirement of data for usage"
    TITLE_2 = "Performance"
    DESCRIPTION_2 = "Higher performance compares to normal multimedia"
    TITLE_3 = "Automation"
    DESCRIPTION_3 = "Automatic in every process, reduce management and action time"


    OurService(title = TITLE_1,description=DESCRIPTION_1).save()
    OurService(title = TITLE_2,description=DESCRIPTION_2).save()
    OurService(title = TITLE_3,description=DESCRIPTION_3).save()



@db.transaction
def create_imas():
    SHORT_DESCRIPTION = "A brief story about how this process works, keep an eye till the end."

    TITLE = "iMAS - Our Brain"
    DESCRIPTION = "iMAS is an independent system, which allow user to automatically send marketing information to customer with higher positive reception. By applying advance technologies being data mining, machine learning, AI, etc. information which is extracted from provided data will contain high quality and personalization for every customer. "
    # TITLE_2 = ""
    # DESCRIPTION_2 = ""
    # TITLE_3 = ""
    # DESCRIPTION_3 = ""

    IMAS(title=TITLE,short_description = SHORT_DESCRIPTION ,description=DESCRIPTION).save()
    # IMAS(title=TITLE_2,short_description = SHORT_DESCRIPTION ,description=DESCRIPTION_2).save()
    # IMAS(title=TITLE_3,short_description = SHORT_DESCRIPTION ,description=DESCRIPTION_3).save()


# @db.transaction
# def create_pricing():
#     DESCRIPTION = ""
#     SHORT_DESCRIPTION = ""
#     OurPricing(short_description=SHORT_DESCRIPTION, description=DESCRIPTION).save()


# @db.transaction
# def create_team():
#     SHORT_DESCRIPTION = ""
#     OurTeam(short_description=SHORT_DESCRIPTION).save()


@db.transaction
def create_contact():
    DESCRIPTION = "Please feel free to contact us if you need further information"
    ADDRESS = "KM29, THANGLONG BOULEVARD, HOALAC HIGH TECH PARK, THACH HOA, THACH THAT,HANOI"
    PHONE_NUMBER = "(+84)967 666 653"
    EMAIL = "dreamers.mas@gmail.com"

    OurContact(short_description= DESCRIPTION, address = ADDRESS,
                         phone_number = PHONE_NUMBER, email = EMAIL).save()


#--run

create_story()
create_service()
create_imas()
# create_pricing()
# create_team()
create_contact()



