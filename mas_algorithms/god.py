from database_context import *
from sklearn.metrics.pairwise import cosine_similarity


import pandas as pd
import numpy as np


config.DATABASE_URL  = 'bolt://neo4j:i-096f10099be71cf0e@bolt://13.251.238.137:7687'

h_dict = get_input_matrix_dictionary()
product_columns = {}
matrix_title = []
all_products = get_all_products()

for i in range(0, len(all_products)):
    matrix_title.append(all_products[i].name)
    col = []
    for k, v in h_dict.items():
        col.append(v[i])
    product_columns[str(all_products[i].name)]=col


data = pd.DataFrame(product_columns)
matrix = cosine_similarity(data.transpose())
print(matrix_title)
print(matrix)

# test_id = get_product_ids_by_con_id("50a013514bce49248f348feaa6d0ed90")
